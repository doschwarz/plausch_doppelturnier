from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from . import cache, parser
import json
import traceback

@require_http_methods(["GET"])
def draw_service(request):
    try:
        force_redraw = 'repeat' in request.GET
        return JsonResponse(cache.get_draw(request.session, force_redraw=force_redraw)[0])
    except RuntimeError as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)

@require_http_methods(["POST"])
def add_player(request):
    try:
        parser.__assert_field_exists('data', request.POST)
        parser.add_player(request.session, json.loads(request.POST['data']))
        return HttpResponse(status=204)

    except (AssertionError, ValueError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)

@require_http_methods(["POST"])
def edit_player(request):
    try:
        parser.__assert_field_exists('data', request.POST)
        parser.edit_player(request.session, json.loads(request.POST['data']))
        return HttpResponse(status=204)

    except (AssertionError, ValueError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)

@require_http_methods(["POST"])
def remove_player(request):
    try:
        parser.__assert_field_exists('name', request.POST)
        parser.remove_player(request.session, request.POST['name'])
        return HttpResponse(status=204)

    except (AssertionError, ValueError, RuntimeError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@require_http_methods(["POST"])
def add_discipline(request):
    try:
        parser.__assert_field_exists('name', request.POST)
        parser.add_discipline(request.session, request.POST['name'])
        return HttpResponse(status=204)

    except (AssertionError, TypeError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@require_http_methods(["POST"])
def remove_discipline(request):
    try:
        parser.__assert_field_exists('name', request.POST)
        parser.remove_discipline(request.session, request.POST['name'])
        return HttpResponse(status=204)

    except (AssertionError, ValueError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@require_http_methods(["POST"])
def enter_result(request):
    try:
        parser.__assert_field_exists('data', request.POST)
        data = json.loads(request.POST['data'])
        parser.save_result(request.session, data)

        template = "history_item.html"
        round_number = len(cache.get_history(request.session)[0])
        context = {"round": data, "number": round_number}

        return render(request, template, context)

    except (AssertionError, ValueError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)


@require_http_methods(["POST"])
def edit_result(request):
    try:
        parser.__assert_field_exists('data', request.POST)
        data = json.loads(request.POST['data'])
        parser.__assert_field_exists('round_number', data)
        parser.edit_result(request.session, data)

        template = "history_item.html"
        context = {"round": data['new_result'], "number": data['round_number']}

        return render(request, template, context)

    except (AssertionError, ValueError) as e:
        traceback.print_exc()
        return HttpResponse(str(e), status=500)
