import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

from engine import Turnier

def parse_score(score):
    scores = [int(n) for n in score.split(" - ")]
    assert len(scores) == 2, "Invalid result '{}'".format(entry['result'])
    diff = scores[0] - scores[1]
    assert diff != 0, "Draws are not allowed, play a decider"
    victor = 1 if diff > 0 else 2

    return victor, abs(diff)

def create_empty_tournament():
    return Turnier().to_dict()

def calculate_ranking(tournament_dict):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    return tournament.calculate_ranking()

def make_draw(tournament_dict):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)

    if len(tournament.disciplines) > 1:
        raw = tournament.make_multi_discipline_draw()
    else:
        raw = tournament.make_draw()
    
    draw = {'doubles': list()}

    if 'bye' in raw:
        draw['bye'] = raw['bye']
    if 'singles' in raw:
        if isinstance(raw['singles'], dict):
            for discipline, (player_1, player_2) in raw['singles'].items():
                draw['singles'] = {'discipline': discipline, 'players': {'s0': player_1, 's1': player_2}}
        else:
            assert isinstance(raw['singles'], list) and len(raw['singles']) == 2, "invalid singles drawn (bug)"
            draw['singles'] = {'discipline': None, 'players': {'s0': raw['singles'][0], 's1': raw['singles'][1]}}
    
    if isinstance(raw['doubles'], dict):
        for discipline, matches in raw['doubles'].items():
            for match in matches:
                draw['doubles'].append({
                    'players': {'t0p0': match[0][0], 't0p1': match[0][1], 't1p0': match[1][0], 't1p1': match[1][1]},
                    'discipline': discipline
                })
    else:
        assert isinstance(raw['doubles'], list), "invalid doubles drawn (bug)"
        for match in raw['doubles']:
                draw['doubles'].append({
                    'players': {'t0p0': match[0][0], 't0p1': match[0][1], 't1p0': match[1][0], 't1p1': match[1][1]},
                    'discipline': None
                })

    return draw

def make_player_list(tournament_dict):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    return tournament.get_player_list()

def get_categories_list(tournament_dict):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    return list(tournament.disciplines)

def add_player(tournament_dict, name, seed):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)    
    tournament.add_player(name, seed)
    return tournament.to_dict()

def edit_player(tournament_dict, old_name, new_name, new_seed): 
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    tournament.edit_player(old_name, new_name, new_seed)
    return tournament.to_dict()

def change_name_in_history(history, old_name, new_name):
    replacer = lambda x: new_name if x == old_name else x
    
    for item in history:
        for match in item['doubles']:
            match['players'] = {k: replacer(v) for k, v in match['players'].items()}
        
        if 'singles' in item:
            item['singles']['players'] = {k: replacer(v) for k, v in  item['singles']['players'].items()}
        
        if 'bye' in item:
            item['bye'] = replacer(item['bye'])
                
    return history


def remove_player(tournament_dict, name):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    tournament.remove_player(name)
    return tournament.to_dict()

def add_discipline(tournament_dict, name):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    tournament.add_discipline(name)
    return tournament.to_dict()

def remove_discipline(tournament_dict, name):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    tournament.remove_discipline(name)
    return tournament.to_dict()


def enter_result(tournament_dict, results):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    for entry in results['doubles']:
        players = entry['players']
        victor, diff = parse_score(entry['result'])
        tournament.enter_result(
            players['t0p0'], players['t0p1'], players['t1p0'], players['t1p1'],
            victor, diff, entry['discipline']
        )

    if 'singles' in results:
        entry = results['singles']
        victor, diff = parse_score(entry['result'])
        tournament.enter_result_Singles(
            entry['players']['s0'], entry['players']['s1'],
            victor, diff, entry['discipline']
        )

    if 'bye' in results:
        tournament.enter_result_Freilos(results['bye'])

    return tournament.to_dict()


def remove_result(tournament_dict, results):
    tournament = Turnier()
    tournament.from_dict(tournament_dict)
    for entry in results['doubles']:
        players = entry['players']
        victor, diff = parse_score(entry['result'])
        tournament.remove_result(
            players['t0p0'], players['t0p1'], players['t1p0'], players['t1p1'],
            victor, diff, entry['discipline']
        )

    if 'singles' in results:
        entry = results['singles']
        victor, diff = parse_score(entry['result'])
        tournament.remove_result_Singles(
            entry['players']['s0'], entry['players']['s1'],
            victor, diff, entry['discipline']
        )

    if 'bye' in results:
        tournament.remove_result_Freilos(results['bye'])

    return tournament.to_dict()
