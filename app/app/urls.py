"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views, services

urlpatterns = [
    # views
    path('', views.index, name='index'),
    path('players/', views.players_tab, name='players_tab'),
    path('draw/', views.draw_tab, name='draw_tab'),
    path('draw/ranking/', views.ranking_service, name='ranking_service'),
    path('reset/', views.reset, name='reset'),

    # services
    path('players/add-player/', services.add_player, name='add_player'),
    path('players/edit-player/', services.edit_player, name='edit_player'),
    path('players/remove-player/', services.remove_player, name='remove_player'),
    path('players/add-discipline/', services.add_discipline, name='add_discipline'),
    path('players/remove-discipline/', services.remove_discipline, name='remove_discipline'),
    path('draw/draw-service/', services.draw_service, name='draw_service'),
    path('draw/enter-result/', services.enter_result, name='enter_result'),
    path('draw/edit-result/', services.edit_result, name='edit_result'),

    # jasmine
    path('jasmine-test-suite/', include('jasmine.urls')),
]
