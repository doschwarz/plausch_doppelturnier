from django.test import TestCase
from django.urls import reverse
import json
import os

class ViewsTests(TestCase):
    def test_players_tab(self):
        url = reverse ("players_tab")
        result = self.client.get(url)
        self.assertEqual(result.status_code, 200)
        
    def test_index(self):
        url = reverse("index")
        result = self.client.get(url)
        self.assertRedirects(result, reverse('players_tab'))
    
    def test_draw_tab(self):
        url = reverse("draw_tab")
        result = self.client.get(url)
        self.assertEqual(result.status_code, 200)
    
    def test_reset(self):
        result = self.client.get(reverse("players_tab"))
        self.assertEqual(result.status_code, 200)
        
        keysBefore = list(self.client.session.keys())
        self.assertGreater(len(keysBefore), 0)

        result = self.client.get(reverse("reset"))
        self.assertEqual(result.status_code, 302)

        keysAfter = list(self.client.session.keys())
        self.assertEqual(len(keysAfter), 0)


class ServicesTests(TestCase):
    def test_add_player(self, nPlayers=2):
        result = self.client.get(reverse("players_tab"))
        self.assertEqual(result.status_code, 200)

        url = reverse("add_player")
        for name, seed in self.__test_players(nPlayers):
            data = {"name": name, "seed": seed}
            result = self.client.post(url, {'data': json.dumps(data)})
            self.assertEqual(result.status_code, 204)
    
    def test_remove_player(self, name=None):
        if name is None:
            self.test_add_player(2)
            name = self.__test_players(1)[0][0]

        url = reverse("remove_player")
        result = self.client.post(url, {"name": name})
        self.assertEqual(result.status_code, 204)

    def test_draw_service(self, write_files=False):
        self.test_add_player(12)
        first3names = [name for name, seed in self.__test_players(3)]
        expectedKeys = [{"doubles"}, {"doubles", "singles", "bye"}, {"doubles", "singles"}, {"doubles", "bye"}]
        url = reverse("draw_service")

        for i in range(4):
            if i > 0:
                self.test_remove_player(first3names[i-1])
            result = self.client.get(url)
            self.assertEqual(result.status_code, 200)

            resultJson = json.loads(result.content)
            self.assertSetEqual(set(resultJson), expectedKeys[i])
            if write_files:
                filename = os.path.join(self.__test_json_folder(), "draw_{:d}.json".format(12-i))
                if os.path.exists(filename):
                    os.remove(filename)
                with open(filename, 'w') as file:
                    json.dump(resultJson, file, allow_nan=False)
        
    def test_enter_result(self):
        self.test_add_player(11)
        result = self.client.get(reverse("draw_service"))
        self.assertEqual(result.status_code, 200)
        draw = json.loads(result.content)
        results = dict()
        for key, value in draw.items():
            if isinstance(value, str):
                results[key] = value
            else:
                self.assertIsInstance(value, list)
                if isinstance(value[0], list):
                    results[key] = [{"players": pair, "result": [1, 2]} for pair in value]
                else:
                    self.assertIsInstance(value[0], str)
                    results[key] = {"players": value, "result": [1, 2]}
        
        result = self.client.post(reverse("enter_result"), {'data': json.dumps(results)})
        self.assertEqual(result.status_code, 200)

    def write_test_files(self):
        self.test_draw_service(write_files=True)

    def __test_json_folder(self):
        cwd = os.getcwd()
        folder = os.path.join(cwd, "static", "test", "json")
        self.assertTrue(os.path.exists(folder))
        return folder

    def __test_players(self, nPlayers):
        self.assertIsInstance(nPlayers, int)
        self.assertGreater(nPlayers, 0)
        testPlayers = [
            ("a", 0), ("b", 1), ("c", 0), ("d", 1), ("e", 0), ("f", 1),
            ("g", 0), ("h", 1), ("i", 0), ("j", 1), ("k", 0), ("l", 1)
        ]
        self.assertLessEqual(nPlayers, len(testPlayers))
        return testPlayers[:nPlayers]
