import json

from . import cache as cacheModule
from . import backend
from .cache import CacheTag


def save_result(cache, data):
    tournament, _ = cacheModule.get_tournament(cache)
    updated = backend.enter_result(tournament, data)
    cache[CacheTag.TOURNAMENT.value] = updated

    if CacheTag.HISTORY.value not in cache:
        cache[CacheTag.HISTORY.value] = [data]
    else:
        cache[CacheTag.HISTORY.value].append(data)


def add_player(cache, data):
    __assert_field_exists('name', data)
    __assert_field_exists('seed', data)
    tournament, _ = cacheModule.get_tournament(cache)
    updated = backend.add_player(tournament, data['name'], int(data['seed']))
    cache[CacheTag.TOURNAMENT.value] = updated


def edit_player(cache, data):
    __assert_field_exists('old_name', data)
    __assert_field_exists('new_name', data)
    __assert_field_exists('new_seed', data)
    tournament, _ = cacheModule.get_tournament(cache)
    updated_player = backend.edit_player(tournament, data['old_name'], data['new_name'], int(data['new_seed']))
    
    if data['old_name'] != data['new_name']:
        history, _ = cacheModule.get_history(cache)
        updated_history = backend.change_name_in_history(history, data['old_name'], data['new_name'])
        cache[CacheTag.HISTORY.value] = updated_history
    
    cache[CacheTag.TOURNAMENT.value] = updated_player


def remove_player(cache, name):
    tournament, _ = cacheModule.get_tournament(cache)
    updated = backend.remove_player(tournament, name)
    cache[CacheTag.TOURNAMENT.value] = updated


def add_discipline(cache, name):
    tournament, _ = cacheModule.get_tournament(cache)
    history, _ = cacheModule.get_history(cache)
    if len(history) > 0:
        raise AssertionError("Discipline cannot be added/removed after tournament has started.")
    
    updated = backend.add_discipline(tournament, name)
    cache[CacheTag.TOURNAMENT.value] = updated


def remove_discipline(cache, name):
    tournament, _ = cacheModule.get_tournament(cache)
    history, _ = cacheModule.get_history(cache)
    if len(history) > 0:
        raise AssertionError("Disciplines cannot be added/removed after tournament has started.")
    
    updated = backend.remove_discipline(tournament, name)
    cache[CacheTag.TOURNAMENT.value] = updated


def edit_result(cache, data):
    __assert_field_exists('old_result', data)
    __assert_field_exists('new_result', data)
    tournament, _ = cacheModule.get_tournament(cache)
    history, _ = cacheModule.get_history(cache)

    found_in_history = False
    old_hash = hash(json.dumps(data['old_result'], sort_keys=True))
    for i, history_item in enumerate(history):
        if hash(json.dumps(history_item, sort_keys=True)) == old_hash:
            if found_in_history:
                raise RuntimeError("Two identical rounds found in history")
            history[i] = data['new_result']
            found_in_history = True
    
    if not found_in_history:
        raise ValueError("Result to be corrected not found in history")
    
    updated = backend.remove_result(tournament, data['old_result'])
    updated = backend.enter_result(updated, data['new_result'])
    cache[CacheTag.TOURNAMENT.value] = updated
    cache[CacheTag.HISTORY.value] = history


def __assert_field_exists(field_name, dict_):
    assert field_name in dict_, "Invalid json body, missing '{}' field".format(field_name)
