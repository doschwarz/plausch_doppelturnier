from django.conf import settings
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.views.decorators.http import require_http_methods
from . import cache
import json

@require_http_methods(["GET"])
def players_tab(request):
    template = 'players.html'
    playerList, _ = cache.get_player_list(request.session)
    categories, _ = cache.get_categories_list(request.session)
    context = {
        "production": not bool(settings.DEBUG),
        "players": playerList['players'],
        "removed": playerList['removed'],
        "categories": categories
    }
    return render(request, template, context)

@require_http_methods(["GET"])
def draw_tab(request):
    template = 'draws.html'
    history, _ = cache.get_history(request.session)
    all_players, _ = cache.get_player_list(request.session)
    all_categories, _ = cache.get_categories_list(request.session)

    context = {
        "production": not bool(settings.DEBUG),
        "use_cache": "cache" in request.GET,
        "history": json.dumps(history),
        "all_players": [p["name"] for p in all_players['players'] + all_players['removed']],
        "all_disciplines": all_categories
    }
    return render(request, template, context)

@require_http_methods(["GET"])
def ranking_service(request):
    ranking = cache.get_ranking(request.session)[0]
    template = 'ranking_table.html'
    context = {"use_cache": "cache" in request.GET, "players": ranking}

    return render(request, template, context)

@require_http_methods(["GET"])
def index(request):
    return redirect('players_tab')

@require_http_methods(["GET"])
def reset(request):
    keys = list(request.session.keys())
    for key in keys:
        del request.session[key]
    return redirect('index')
