"""
WSGI config for app project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os, sys
THIS_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(THIS_DIR)
sys.path.append(os.path.join(THIS_DIR, '..'))
sys.path.append(os.path.join(THIS_DIR, '..', '..'))
sys.path.append(os.path.join(THIS_DIR, '..', '..', 'antenv', 'lib', 'python3.7', 'site-packages'))

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'azure_settings')

application = get_wsgi_application()
