from django.apps import AppConfig

class WebGuiConfig(AppConfig):
    name = 'app'
