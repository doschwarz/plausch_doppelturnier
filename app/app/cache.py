from enum import Enum
import json
import copy
from . import backend

class CacheTag(Enum):
    TOURNAMENT = "turnier"
    RANKING = "ranking"
    PLAYERLIST = "playerList"
    CATEGORIES = "categories"
    DRAW = "draw"
    HISTORY = "history"


def __hash_tag(tag):
    """
    :param tag: (str)
    """
    return tag + "_hash"


def get_tournament(cache):
    tag = CacheTag.TOURNAMENT.value
    if tag not in cache:
        tournament = backend.create_empty_tournament()
        cache[CacheTag.TOURNAMENT.value] = tournament
    else:
        tournament = copy.deepcopy(cache[tag])

    return tournament, hash(json.dumps(tournament, sort_keys=True))


def get_history(cache):
    tag = CacheTag.HISTORY.value
    if tag not in cache:
        history = list()
        cache[tag] = history
    else:
        history = cache[tag]

    return history, hash(json.dumps(history, sort_keys=True))


def get_ranking(cache):
    tag = CacheTag.RANKING.value
    tournament, tournament_hash = get_tournament(cache)
    prev_hash = cache.get(__hash_tag(tag), 0)
    if tournament_hash != prev_hash:
        ranking = backend.calculate_ranking(tournament)
        cache[tag] = ranking
        cache[__hash_tag(tag)] = tournament_hash
    else:
        assert tag in cache
        ranking = cache[tag]

    return ranking, hash(json.dumps(ranking, sort_keys=True))


def get_draw(cache, force_redraw=False):
    tag = CacheTag.DRAW.value
    tournament, tournament_hash = get_tournament(cache)
    prev_hash = cache.get(__hash_tag(tag), 0)
    if force_redraw or (tournament_hash != prev_hash):
        draw = backend.make_draw(tournament)
        cache[tag] = draw
        cache[__hash_tag(tag)] = tournament_hash
    else:
        assert tag in cache
        draw = cache[tag]

    return draw, hash(json.dumps(draw, sort_keys=True))


def get_player_list(cache):
    tag = CacheTag.PLAYERLIST.value
    tournament, tournament_hash = get_tournament(cache)
    prev_hash = cache.get(__hash_tag(tag), 0)
    if tournament_hash != prev_hash:
        player_list = backend.make_player_list(tournament)
        cache[tag] = player_list
        cache[__hash_tag(tag)] = tournament_hash
    else:
        assert tag in cache
        player_list = cache[tag]

    return player_list, hash(json.dumps(player_list, sort_keys=True))


def get_categories_list(cache):
    tag = CacheTag.CATEGORIES.value
    tournament, tournament_hash = get_tournament(cache)
    prev_hash = cache.get(__hash_tag(tag), 0)
    if tournament_hash != prev_hash:
        categories_list = backend.get_categories_list(tournament)
        cache[tag] = categories_list
        cache[__hash_tag(tag)] = tournament_hash
    else:
        assert tag in cache
        categories_list = cache[tag]
    
    return categories_list, hash(json.dumps(categories_list, sort_keys=True))
