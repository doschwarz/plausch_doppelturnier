import logging
import os

from django.conf import settings
from django.views.generic.base import TemplateView

import json as simplejson

logger = logging.getLogger("jasmine")


class DjangoJasmineView(TemplateView):
    template_name = "jasmine/base.html"

    def get_context_data(self, version=None):
        """Run the jasmine tests and render index.html"""
        root = settings.JASMINE_TEST_DIRECTORY
        # Get all files in spec dir and subdirs
        all_files = []
        for curpath, dirs, files in os.walk(os.path.join(root, "spec")):
            for name in files:
                if not name.startswith("."):
                    "We want to avoid .file.js.swp and co"
                    curpath = curpath.replace(os.path.join(root, "spec"), "")
                    all_files.append(os.path.join(curpath, name))

        suite = {}
        # defaults
        suite['js_files'] = []
        suite['static_files'] = []

        # load files.json if present
        if os.path.exists(os.path.join(root, "files.json")):
            file = open(os.path.join(root, 'files.json'), 'r')
            json = file.read()
            try:
                json = simplejson.loads(json)
            except ValueError:
                logger.info("You might have a syntax error in your files.json, "
                    "like a surplus comma")
                # Trick to call back the django handler500, couldn't find a way to
                # customize the Exception Type field in the debug Traceback
                json = simplejson.loads(json)
            suite.update(json)
            file.close()

        data = {
            'files': [fl for fl in all_files if fl.endswith('js')],
            'suite': suite,
        }
        return data
