from django.apps import AppConfig


class DjangoJasmineConfig(AppConfig):
    name = "jasmine"
    verbose_name = "Django Jasmine"
