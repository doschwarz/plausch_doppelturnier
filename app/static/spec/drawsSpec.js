describe("populate_matches_list", function() {
    let matches;

    beforeAll(function () {
        jasmine.getFixtures().fixturesPath = '/';
        jasmine.getJSONFixtures().fixturesPath = '/static/test/json';
    });

    beforeEach(function () {
        loadFixtures('draw/');
    });

    it("should work with only doubles", function() {
        matches = getJSONFixture('draw_12.json');
        populate_matches_list(matches);

        expect($("#doubles")).toBeVisible();
        expect($("#doubles-list > li")).toHaveLength(3);
        let text;
        let name;
        let element;
        $.each(matches.doubles, function(){
            text = this[0][0] + " + " + this[0][1] + " vs. " + this[1][0] + " + " + this[1][1];
            name = JSON.stringify(this);
            element = $("[name='" + name + "']");
            expect(element).toExist();
            expect(element).toHaveValue(" - ");
            expect(element).toHaveClass("doubles-input");
            expect(element.parent()).toContainText(text);

        });
        
        expect($("#singles")).toBeHidden();
        expect($("#bye")).toBeHidden();
    });

    it("should work with doubles + bye", function(){
        matches = getJSONFixture('draw_9.json');
        populate_matches_list(matches);

        expect($("#doubles")).toBeVisible();
        expect($("#doubles-list > li")).toHaveLength(2);

        expect($("#singles")).toBeHidden();
        expect($("#bye")).toBeVisible();
        expect($("#bye-span")).toContainText(matches.bye);
    });

    it("should work with doubles + singles", function(){
        matches = getJSONFixture('draw_10.json');
        populate_matches_list(matches);

        expect($("#doubles")).toBeVisible();
        expect($("#doubles-list > li")).toHaveLength(2);

        expect($("#bye")).toBeHidden();
        expect($("#singles")).toBeVisible();
        expect($("#singles-input")).toHaveAttr("name", JSON.stringify(matches.singles));
        expect($("#singles-input")).toHaveValue(" - ");
        expect($("#singles-span")).toContainText(matches.singles[0] + " vs. " + matches.singles[1])
    });
    it("should work with doubles + singles + bye", function(){
        matches = getJSONFixture('draw_11.json');
        populate_matches_list(matches);

        expect($("#doubles")).toBeVisible();
        expect($("#doubles-list > li")).toHaveLength(2);

        expect($("#singles")).toBeVisible();
        expect($("#bye")).toBeVisible();
    });
});

describe("check_int", function(){
    let check;
    it("should accept '2'", function(){
        check = check_int('2');
        expect(check).toBeTruthy();
    });
    it("should accept '-2897'", function(){
        check = check_int('-2897');
        expect(check).toBeTruthy();
    });
    it("should reject '2r'", function(){
        check = check_int('2r');
        expect(check).toBeFalsy();
    });
    it("should reject '2 5'", function(){
        check = check_int('2 5');
        expect(check).toBeFalsy();
    });
});

describe("parse_result", function(){
    let result;
    it("should accept '1-2'", function(){
        result = parse_result('1-2');
        expect(result).toEqual([[1, 2], true]); 
    });
    it("should accept ' 12 - 987 '", function(){
        result = parse_result(' 12 - 987 ');
        expect(result).toEqual([[12, 987], true]); 
    });
    it("should reject '5:3'", function(){
        result = parse_result('5:3');
        expect(result[1]).toBeFalsy(); 
    });
});

describe("collect_results", function () {
    let matches;
    let results;
    let doubles;
    let singles;
    beforeAll(function () {
        jasmine.getFixtures().fixturesPath = '/';
        jasmine.getJSONFixtures().fixturesPath = '/static/test/json';
    });

    beforeEach(function () {
        loadFixtures('draw/');
    });

    it("should work with singles + doubles + bye", function(){
        matches = getJSONFixture('draw_11.json');
        populate_matches_list(matches);
        $(".doubles-input").val("12-15");
        $("#singles-input").val("18-14");
        results = collect_results();

        expect(results).toHaveLength(2);
        expect(results[1]).toBeTruthy();
        expect(Object.keys(results[0])).toContain("doubles");
        doubles = JSON.parse(results[0].doubles);
        $.each(doubles, function(){
            expect(Object.keys(this)).toContain("players");
            expect(matches.doubles).toContain(this.players);
            expect(Object.keys(this)).toContain("result");
            expect(this.result).toEqual([12, 15]);
        });

        expect(Object.keys(results[0])).toContain("singles");
        singles = JSON.parse(results[0].singles);
        expect(Object.keys(singles)).toContain("players");
        expect(singles.players).toEqual(matches.singles);
        expect(Object.keys(singles)).toContain("result");
        expect(singles.result).toEqual([18, 14]);

        expect(Object.keys(results[0])).toContain("bye");
        expect(results[0].bye).toEqual(matches.bye);
    });

    it("should work with only doubles", function (){
        matches = getJSONFixture('draw_12.json');
        populate_matches_list(matches);
        $(".doubles-input").val("12 - 15");
        results = collect_results();

        expect(results).toHaveLength(2);
        expect(results[1]).toBeTruthy();
        expect(Object.keys(results[0])).toContain("doubles");
        expect(Object.keys(results[0])).not.toContain("singles");
        expect(Object.keys(results[0])).not.toContain("bye");
    });

    it("should work with doubles + bye", function (){
        matches = getJSONFixture('draw_9.json');
        populate_matches_list(matches);
        $(".doubles-input").val("12 - 15");
        results = collect_results();

        expect(results).toHaveLength(2);
        expect(results[1]).toBeTruthy();
        expect(Object.keys(results[0])).toContain("doubles");
        expect(Object.keys(results[0])).not.toContain("singles");
        expect(Object.keys(results[0])).toContain("bye");
    });

    it("should work with doubles + singles", function (){
        matches = getJSONFixture('draw_10.json');
        populate_matches_list(matches);
        $(".doubles-input").val("12 - 15");
        $("#singles-input").val("18-14");
        results = collect_results();

        expect(results).toHaveLength(2);
        expect(results[1]).toBeTruthy();
        expect(Object.keys(results[0])).toContain("doubles");
        expect(Object.keys(results[0])).toContain("singles");
        expect(Object.keys(results[0])).not.toContain("bye");
    });
});
