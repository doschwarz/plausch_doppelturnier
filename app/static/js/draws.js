var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function showErrorModal(label, error_text) {
    $("#errorModalLabel").text(label);
    $("#errorModalText").text(error_text);
    $('#errorModal').modal('show');
}

function show_ranking(url) {
    $.get({
        url: url,
        success: function success(result) {
            $("#ranking-table").html(result);
        },
        error: function error(result) {
            showErrorModal("Calculating ranking failed", "Error: " + result.responseText);
        }
    });
}

function check_int(intString) {
    return String(parseInt(String(intString))) === String(intString);
}

function parse_result(resultString) {
    var digits = resultString.replace(/\s+/g, '').split("-");
    if (digits.length !== 2) {
        return [digits, false];
    }
    if (check_int(digits[0]) && check_int(digits[1])) {
        var d0 = parseInt(digits[0]);
        var d1 = parseInt(digits[1]);
        return [[d0, d1], d0 !== d1];
    }
    return [digits, false];
}

/**
 * props
 * -----
 * value:               displayed value
 * mutable (bool):      make a <select> or just return value
 * all_values (Array):  all discipline names
 * onChange (function): handle for changes of <select> value
 */
function Select(props) {
    if (props.mutable) {
        var options = props.all_values.map(function (value, index) {
            return React.createElement(
                "option",
                {
                    key: index,
                    value: value
                },
                value
            );
        });

        return React.createElement(
            "select",
            {
                onChange: props.onChange,
                value: props.value
            },
            options
        );
    } else {
        return props.value;
    }
}

/**
 * props
 * -----
 * discipline (str):            discipline of match or null, then discipline column will not be displayed
 * mutable_discipline (bool)
 * all_disciplines (Array):     all disciplines
 * changeDiscipline (function): (newDiscipline) -> void
 * 
 * players (Object):            player names; keys : {t0p0, t0p1, t1p0, t1p1} or {s0, s1}
 * mutable_players (bool)
 * all_players (Array):         all players including " - "
 *                              in the order they should appear in the <select> tags
 * changeName (function):       (playerKey, newName) -> void
 * 
 * result (str):                result or null, then result column will not be displayed
 * mutable_result (bool)
 * changeResult (function):     (newResult) -> void
 */
function Match(props) {

    function handleDisciplineChange(event) {
        event.preventDefault();
        props.changeDiscipline(event.target.value);
    }

    function handleNameChange(event, playerKey) {
        event.preventDefault();
        props.changeName(playerKey, event.target.value);
    }

    function handleResultChange(event) {
        event.preventDefault();
        props.changeResult(event.target.value);
    }

    var disciplineCol = null;
    if (props.discipline) {
        disciplineCol = React.createElement(
            "div",
            { className: "col-sm-2" },
            React.createElement(
                "p",
                null,
                React.createElement(Select, {
                    value: props.discipline,
                    mutable: props.mutable_discipline,
                    all_values: props.all_disciplines,
                    onChange: function onChange(event) {
                        return handleDisciplineChange(event);
                    }
                }),
                ":"
            )
        );
    }

    var players = {};

    var _loop = function _loop(_ref) {
        _ref2 = _slicedToArray(_ref, 2);
        var playerKey = _ref2[0];
        var name = _ref2[1];

        players[playerKey] = React.createElement(Select, {
            value: name,
            mutable: props.mutable_players,
            all_values: props.all_players,
            onChange: function onChange(event) {
                return handleNameChange(event, playerKey);
            }
        });
    };

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = Object.entries(props.players)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var _ref = _step.value;

            var _ref2;

            _loop(_ref);
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    var team0 = players.s0 || React.createElement(
        React.Fragment,
        null,
        players.t0p0,
        " + ",
        players.t0p1
    );
    var team1 = players.s1 || React.createElement(
        React.Fragment,
        null,
        players.t1p0,
        " + ",
        players.t1p1
    );

    var resultCol = null;
    if (props.result) {
        if (props.mutable_result) {
            resultCol = React.createElement(
                "div",
                { className: "col-sm-2" },
                React.createElement("input", {
                    type: "text",
                    value: props.result,
                    className: "doubles-input",
                    onChange: function onChange(event) {
                        return handleResultChange(event);
                    }
                })
            );
        } else {
            resultCol = React.createElement(
                "div",
                { className: "col-sm-2" },
                React.createElement(
                    "p",
                    null,
                    props.result
                )
            );
        }
    }

    var col_width = String(4 + (disciplineCol === null) + (resultCol === null));

    return React.createElement(
        "div",
        { className: "row" },
        disciplineCol,
        React.createElement(
            "div",
            { className: 'col-sm-' + col_width },
            React.createElement(
                "p",
                null,
                team0,
                React.createElement(
                    "span",
                    { style: { float: 'right' } },
                    " vs. "
                )
            )
        ),
        React.createElement(
            "div",
            { className: 'col-sm-' + col_width },
            React.createElement(
                "p",
                null,
                team1
            )
        ),
        resultCol
    );
}

/**
 * props
 * -----
 * matches (Object):        {doubles: [{players: {t0p0: (str), t0p1: (str), t1p0: (str), t1p1: (str)},
 *                                      result: (str),
 *                                      discipline: (str) or null },
 *                                      ... ],
 *                           singles: {players: {s0: (str), s1: (str)},
 *                                     result: (str),
 *                                     discipline: (str) or null },
 *                           bye: (str) }
 * 
 * mutable_players (bool)
 * all_players (Array):     (str) all player names
 * changeName (function):   (index, playerKey, newName) -> void
 * 
 * mutable_disciplines (bool)
 * all_disciplines (Array):     (str) all discipline names
 * changeDiscipline (function): (index, newDiscipline) -> void 
 * 
 * show_results (bool)
 * mutable_results (bool)
 * changeResult (function):     (index, newResult)
 * 
 * %where (index) is {0, 1, ...} for doubles matches, 'singles' or 'bye'
 */
function MatchesList(props) {

    var doubles_rows = props.matches.doubles.map(function (match, index) {
        return React.createElement(Match, {
            key: index,
            discipline: match.discipline,
            mutable_discipline: props.mutable_disciplines,
            all_disciplines: props.all_disciplines,
            changeDiscipline: function changeDiscipline(d) {
                return props.changeDiscipline(index, d);
            },

            players: match.players,
            mutable_players: props.mutable_players,
            all_players: props.all_players,
            changeName: function changeName(k, n) {
                return props.changeName(index, k, n);
            },

            result: props.show_results ? match.result || " - " : null,
            mutable_result: props.mutable_results,
            changeResult: function changeResult(r) {
                return props.changeResult(index, r);
            }
        });
    });

    var singles_rows = null;
    if ('singles' in props.matches) {
        var match = props.matches.singles;
        singles_rows = React.createElement(
            React.Fragment,
            null,
            React.createElement(
                "div",
                { className: "row" },
                React.createElement(
                    "div",
                    { className: "col-sm-2" },
                    React.createElement(
                        "p",
                        null,
                        React.createElement(
                            "b",
                            null,
                            "Singles:"
                        )
                    )
                )
            ),
            React.createElement(Match, {
                discipline: match.discipline,
                mutable_discipline: props.mutable_disciplines,
                all_disciplines: props.all_disciplines,
                changeDiscipline: function changeDiscipline(d) {
                    return props.changeDiscipline("singles", d);
                },

                players: match.players,
                mutable_players: props.mutable_players,
                all_players: props.all_players,
                changeName: function changeName(k, n) {
                    return props.changeName("singles", k, n);
                },

                result: props.show_results ? match.result || " - " : null,
                mutable_result: props.mutable_results,
                changeResult: function changeResult(r) {
                    return props.changeResult("singles", r);
                }
            })
        );
    }

    var bye_row = !('bye' in props.matches) ? null : React.createElement(
        "div",
        { className: "row" },
        React.createElement(
            "div",
            { className: "col-sm-12" },
            React.createElement(
                "p",
                null,
                React.createElement(
                    "b",
                    null,
                    "Bye: "
                ),
                React.createElement(Select, {
                    value: props.matches.bye,
                    mutable: props.mutable_players,
                    all_values: props.all_players,
                    onChange: function onChange(event) {
                        event.preventDefault();
                        props.changeName("bye", null, event.target.value);
                    }
                })
            )
        )
    );

    return React.createElement(
        React.Fragment,
        null,
        React.createElement(
            "div",
            { className: "row" },
            React.createElement(
                "div",
                { className: "col-sm-2" },
                React.createElement(
                    "p",
                    null,
                    React.createElement(
                        "b",
                        null,
                        "Doubles:"
                    )
                )
            )
        ),
        doubles_rows,
        singles_rows,
        bye_row
    );
}

/**
 * props
 * -----
 * draw_service_url (str)
 * ranking_url (str)
 * submit_url (str)
 * round_number (int)
 * all_players (Array):         list of all player names
 * all_disciplines (Array):     list of all discipline names
 * add_to_history (function):   (match) -> void
 */

var Draw = function (_React$Component) {
    _inherits(Draw, _React$Component);

    function Draw(props) {
        _classCallCheck(this, Draw);

        var _this = _possibleConstructorReturn(this, (Draw.__proto__ || Object.getPrototypeOf(Draw)).call(this, props));

        _this.state = {
            matches: null,
            round_number: _this.props.round_number || 1,
            data_ok: null,
            show_modify_draw_modal: false
        };
        return _this;
    }

    _createClass(Draw, [{
        key: "changeResult",
        value: function changeResult(index, newResult) {
            var data = parse_result(newResult);
            var new_matches = JSON.parse(JSON.stringify(this.state.matches));
            if (data[1]) {
                newResult = String(data[0][0]) + " - " + String(data[0][1]);
            }

            if (index === "singles") {
                new_matches.singles['result'] = newResult;
            } else {
                // TODO: assert index is number in [0, array.length]
                new_matches.doubles[index]['result'] = newResult;
            }
            var tmp = {};
            tmp[index] = data[1];
            this.setState({
                matches: new_matches,
                data_ok: Object.assign(this.state.data_ok, tmp)
            });
        }
    }, {
        key: "requestDraw",
        value: function requestDraw() {
            var _this2 = this;

            var repeatDraw = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            $.get({
                url: this.props.draw_service_url + (repeatDraw ? "?repeat" : ""),
                success: function success(result) {
                    console.log("match received from server", result);
                    var data_ok = {};
                    if ('singles' in result) {
                        data_ok['singles'] = false;
                    }
                    result.doubles.forEach(function (val, index) {
                        data_ok[index] = false;
                    });
                    _this2.setState({ matches: result, data_ok: data_ok });
                },
                error: function error(result) {
                    $("#errorModalLabel").text("Making draw failed");
                    $("#errorModalText").text("Error: " + result.responseText);
                    $('#errorModal').modal('show');
                }
            });
        }
    }, {
        key: "submitResults",
        value: function submitResults(event) {
            var _this3 = this;

            event.preventDefault();
            var wrong_data = null;
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = Object.entries(this.state.data_ok)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var _ref3 = _step2.value;

                    var _ref4 = _slicedToArray(_ref3, 2);

                    var key = _ref4[0];
                    var data_ok = _ref4[1];

                    if (!data_ok) {
                        wrong_data = check_int(key) ? String(parseInt(key) + 1) + ". doubles" : key;
                        break;
                    }
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            if (wrong_data) {
                $("#errorModalLabel").text("Error: Incorrect results");
                $("#errorModalText").text("Result of " + wrong_data + " not entered correctly");
                $('#errorModal').modal('show');
            } else {
                console.log("results sent to server", this.state.matches);
                $.post({
                    url: this.props.submit_url,
                    data: {
                        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                        data: JSON.stringify(this.state.matches)
                    },
                    success: function success() {
                        console.log("Successfully submitted result");
                        _this3.props.add_to_history(_this3.state.matches);
                        _this3.setState({
                            round_number: _this3.state.round_number + 1,
                            matches: null,
                            data_ok: null
                        });
                        show_ranking(_this3.props.ranking_url);
                        _this3.requestDraw();
                    },
                    error: function error(result) {
                        $("#errorModalLabel").text("Submitting results failed");
                        $("#errorModalText").text("Error: " + result.responseText);
                        $('#errorModal').modal('show');
                    }
                });
            }
        }
    }, {
        key: "render",
        value: function render() {
            var _this4 = this;

            var matchIsNull = this.state.matches ? false : true;
            var modal_id = "change-current-draw-modal";

            var drawRows = matchIsNull ? null : React.createElement(
                React.Fragment,
                null,
                React.createElement(
                    "div",
                    { className: "row" },
                    React.createElement(
                        "div",
                        { className: "col-sm-5" },
                        React.createElement(
                            "h3",
                            null,
                            "Round ",
                            this.state.round_number
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-sm-2" },
                        React.createElement(
                            "button",
                            {
                                type: "button",
                                className: "btn btn-outline-secondary",
                                onClick: function onClick(event) {
                                    event.preventDefault();
                                    _this4.setState({ show_modify_draw_modal: true }, function () {
                                        $("#" + modal_id).modal('show');
                                    });
                                }
                            },
                            "\u270E"
                        )
                    )
                ),
                React.createElement(
                    "form",
                    { onSubmit: function onSubmit(event) {
                            return _this4.submitResults(event);
                        } },
                    React.createElement(MatchesList, {
                        matches: this.state.matches,
                        mutable_players: false,
                        mutable_disciplines: false,
                        show_results: true,
                        mutable_results: true,
                        changeResult: function changeResult(i, r) {
                            return _this4.changeResult(i, r);
                        }
                    }),
                    React.createElement(
                        "div",
                        { className: "row" },
                        React.createElement(
                            "button",
                            {
                                type: "submit",
                                className: "btn btn-info"
                            },
                            "Submit results"
                        )
                    )
                )
            );

            return React.createElement(
                React.Fragment,
                null,
                React.createElement(
                    "div",
                    { className: "row", style: { paddingBottom: "10px" } },
                    React.createElement(
                        "button",
                        {
                            type: "button",
                            className: "btn btn-info",
                            onClick: function onClick(event) {
                                event.preventDefault();
                                _this4.requestDraw(!matchIsNull);
                            }
                        },
                        matchIsNull ? "Draw next round" : "Repeat draw"
                    )
                ),
                drawRows,
                React.createElement(EditResultsModal, {
                    key: this.state.show_modify_draw_modal || 0,
                    matches: this.state.show_modify_draw_modal ? this.state.matches : null,
                    all_players: this.props.all_players,
                    all_disciplines: this.props.all_disciplines,
                    show_results: false,
                    modalId: modal_id,
                    title: "Change current draw",
                    callback: function callback(matches) {
                        _this4.setState({ matches: matches });
                    }
                })
            );
        }
    }]);

    return Draw;
}(React.Component);

function initDataOk(matches) {
    var data_ok = {};
    if ('bye' in matches) {
        data_ok['bye'] = {
            name: true
        };
    }
    if ('singles' in matches) {
        data_ok['singles'] = {
            result: true,
            s0: true,
            s1: true
        };
    }
    matches.doubles.forEach(function (val, index) {
        data_ok[index] = {
            result: true,
            t0p0: true,
            t0p1: true,
            t1p0: true,
            t1p1: true
        };
    });

    return data_ok;
}

/**
 * props
 * -----
 * matches (Object):        matches object or null
 * all_players (Array):     list of all player names
 * all_disciplines (Array): list of all discipline names
 * show_results (bool):     also make results editable (not displayed otherwise)
 * modalId (str):           id of modal
 * title (str):             displayed modal title
 * callback (function):     (matches) -> void
 */

var EditResultsModal = function (_React$Component2) {
    _inherits(EditResultsModal, _React$Component2);

    function EditResultsModal(props) {
        _classCallCheck(this, EditResultsModal);

        var _this5 = _possibleConstructorReturn(this, (EditResultsModal.__proto__ || Object.getPrototypeOf(EditResultsModal)).call(this, props));

        var all_players = props.all_players.slice(0);
        all_players.unshift("-");
        _this5.state = {
            matches: props.matches ? JSON.parse(JSON.stringify(props.matches)) : null,
            all_players: all_players,
            data_ok: props.matches ? initDataOk(props.matches) : null
        };
        return _this5;
    }

    _createClass(EditResultsModal, [{
        key: "changeResult",
        value: function changeResult(index, newResult) {
            var data = parse_result(newResult);
            if (data[1]) {
                newResult = String(data[0][0]) + " - " + String(data[0][1]);
            }

            var new_matches = JSON.parse(JSON.stringify(this.state.matches));
            if (index === "singles") {
                new_matches.singles.result = newResult;
            } else {
                // TODO: assert index is number in [0, array.length]
                new_matches.doubles[index].result = newResult;
            }

            var new_data_ok = JSON.parse(JSON.stringify(this.state.data_ok));
            new_data_ok[index].result = data[1];
            this.setState({
                matches: new_matches,
                data_ok: new_data_ok
            });
        }
    }, {
        key: "changeName",
        value: function changeName(index, playerKey, newName) {
            var _this6 = this;

            // TODO: assert newName in this.props.all_players
            var new_name_not_dash = newName !== "-";

            var new_matches = JSON.parse(JSON.stringify(this.state.matches));
            var new_data_ok = JSON.parse(JSON.stringify(this.state.data_ok));
            var new_all_players = this.state.all_players.slice(0);

            var oldName = void 0;
            if (index === "bye") {
                oldName = this.state.matches.bye;
                new_matches.bye = newName;
                new_data_ok.bye.name = new_name_not_dash;
            } else if (index === "singles") {
                oldName = this.state.matches.singles.players[playerKey];
                new_matches.singles.players[playerKey] = newName;
                new_data_ok.singles[playerKey] = new_name_not_dash;
            } else {
                oldName = this.state.matches.doubles[index].players[playerKey];
                new_matches.doubles[index].players[playerKey] = newName;
                new_data_ok[index][playerKey] = new_name_not_dash;
            }

            var old_name_not_dash = oldName !== "-";

            if (new_name_not_dash) {
                this.state.matches.doubles.forEach(function (match, i) {
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = Object.entries(match.players)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var _ref5 = _step3.value;

                            var _ref6 = _slicedToArray(_ref5, 2);

                            var k = _ref6[0];
                            var name = _ref6[1];

                            if (name === newName && (i !== index || k !== playerKey)) {
                                new_matches.doubles[i].players[k] = "-";
                                new_data_ok[i][k] = false;
                                if (old_name_not_dash) {
                                    new_all_players.splice(_this6.state.all_players.indexOf(oldName), 1);
                                    new_all_players.unshift(oldName);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }
                });

                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = Object.entries(this.state.matches.singles.players)[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var _ref7 = _step4.value;

                        var _ref8 = _slicedToArray(_ref7, 2);

                        var k = _ref8[0];
                        var name = _ref8[1];

                        if (name === newName && k !== playerKey) {
                            new_matches.singles.players[k] = "-";
                            new_data_ok.singles[k] = false;
                            if (old_name_not_dash) {
                                new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                                new_all_players.unshift(oldName);
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }

                if (this.state.matches.bye === newName && index !== "bye") {
                    new_matches.bye = "-";
                    new_data_ok.bye.name = false;
                    if (old_name_not_dash) {
                        new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                        new_all_players.unshift(oldName);
                    }
                }

                if (oldName === "-") {
                    new_all_players.splice(this.state.all_players.indexOf(newName), 1);
                    new_all_players.push(newName);
                }
            } else if (old_name_not_dash) {
                new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                new_all_players.unshift(oldName);
            }

            this.setState({
                matches: new_matches,
                data_ok: new_data_ok,
                all_players: new_all_players
            });
        }
    }, {
        key: "changeDiscipline",
        value: function changeDiscipline(index, newDiscipline) {
            // TODO: assert discipline is in all_disciplines
            var new_matches = JSON.parse(JSON.stringify(this.state.matches));
            if (index === "singles") {
                new_matches.singles.discipline = newDiscipline;
            } else {
                // TODO: assert index is number in [0, array.length]
                new_matches.doubles[index].discipline = newDiscipline;
            }
            this.setState({ matches: new_matches });
        }
    }, {
        key: "applyChanges",
        value: function applyChanges(event) {
            event.preventDefault();
            var wrong_data = null;
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
                for (var _iterator5 = Object.entries(this.state.data_ok)[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                    var _ref9 = _step5.value;

                    var _ref10 = _slicedToArray(_ref9, 2);

                    var key = _ref10[0];
                    var data = _ref10[1];
                    var _iteratorNormalCompletion6 = true;
                    var _didIteratorError6 = false;
                    var _iteratorError6 = undefined;

                    try {
                        for (var _iterator6 = Object.entries(data)[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                            var _ref11 = _step6.value;

                            var _ref12 = _slicedToArray(_ref11, 2);

                            var key2 = _ref12[0];
                            var data_ok = _ref12[1];

                            if (!data_ok) {
                                wrong_data = (key2 === "result" ? "Result of " : "Name in ") + (check_int(key) ? String(parseInt(key) + 1) + ". doubles" : key);
                                break;
                            }
                        }
                    } catch (err) {
                        _didIteratorError6 = true;
                        _iteratorError6 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                _iterator6.return();
                            }
                        } finally {
                            if (_didIteratorError6) {
                                throw _iteratorError6;
                            }
                        }
                    }

                    if (wrong_data) {
                        break;
                    }
                }
            } catch (err) {
                _didIteratorError5 = true;
                _iteratorError5 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion5 && _iterator5.return) {
                        _iterator5.return();
                    }
                } finally {
                    if (_didIteratorError5) {
                        throw _iteratorError5;
                    }
                }
            }

            if (wrong_data) {
                $("#errorModalLabel").text("Error: Incorrect results");
                $("#errorModalText").text(wrong_data + " is invalid.");
                $('#errorModal').modal('show');
            } else {
                $('#' + this.props.modalId).modal('hide');
                this.props.callback(this.state.matches);
            }
        }
    }, {
        key: "render",
        value: function render() {
            var _this7 = this;

            var matchesList = this.state.matches ? React.createElement(MatchesList, {
                matches: this.state.matches,
                mutable_players: true,
                all_players: this.state.all_players,
                changeName: function changeName(i, k, n) {
                    return _this7.changeName(i, k, n);
                },

                mutable_disciplines: true,
                all_disciplines: this.props.all_disciplines,
                changeDiscipline: function changeDiscipline(i, d) {
                    return _this7.changeDiscipline(i, d);
                },

                show_results: this.props.show_results,
                mutable_results: true,
                changeResult: function changeResult(i, r) {
                    return _this7.changeResult(i, r);
                }
            }) : null;
            return React.createElement(
                "div",
                { className: "modal fade", id: this.props.modalId, tabIndex: "-1", role: "dialog", "aria-labelledby": this.props.modalId + "-title" },
                React.createElement(
                    "div",
                    { className: "modal-dialog modal-lg modal-dialog-centered", role: "document" },
                    React.createElement(
                        "div",
                        { className: "modal-content" },
                        React.createElement(
                            "div",
                            { className: "modal-header" },
                            React.createElement(
                                "h4",
                                { className: "modal-title", id: this.props.modalId + "-title" },
                                this.props.title
                            ),
                            React.createElement(
                                "button",
                                { type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close" },
                                React.createElement(
                                    "span",
                                    { "aria-hidden": "true" },
                                    "\xD7"
                                )
                            )
                        ),
                        React.createElement(
                            "form",
                            { onSubmit: function onSubmit(event) {
                                    return _this7.applyChanges(event);
                                } },
                            React.createElement(
                                "div",
                                { className: "modal-body" },
                                matchesList
                            ),
                            React.createElement(
                                "div",
                                { className: "modal-footer" },
                                React.createElement(
                                    "button",
                                    { type: "button", className: "btn btn-default", "data-dismiss": "modal" },
                                    "Cancel"
                                ),
                                React.createElement(
                                    "button",
                                    { type: "submit", className: "btn btn-primary" },
                                    "Confirm"
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return EditResultsModal;
}(React.Component);

/**
 * props
 * -----
 * rounds (Array):          list of results (matches Objects)
 * edit_modal_id (str)      
 * edit_round (function):   (int) -> void
 */


function History(props) {
    var left_column = [];
    var right_column = [];

    function editRound(event, index) {
        event.preventDefault();
        props.edit_round(index);
    }

    props.rounds.forEach(function (round, index) {
        var item = React.createElement(
            "div",
            { className: "row", key: index },
            React.createElement(
                "div",
                { className: "col-sm-12" },
                React.createElement(
                    "div",
                    { className: "row" },
                    React.createElement(
                        "div",
                        { className: "col-sm-3" },
                        React.createElement(
                            "h3",
                            { className: "history-round-number" },
                            "Round ",
                            index + 1
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "col-sm-1" },
                        React.createElement(
                            "button",
                            {
                                type: "button",
                                className: "btn btn-outline-secondary",
                                onClick: function onClick(event) {
                                    return editRound(event, index);
                                }
                            },
                            "\u270E"
                        )
                    )
                ),
                React.createElement(MatchesList, {
                    matches: round,
                    mutable_players: false,
                    mutable_disciplines: false,
                    show_results: true,
                    mutable_results: false
                })
            )
        );
        if (index % 2) {
            right_column.push(item);
        } else {
            left_column.push(item);
        }
    });

    return React.createElement(
        "div",
        { className: "row" },
        React.createElement(
            "div",
            { className: "col-sm-6" },
            left_column
        ),
        React.createElement(
            "div",
            { className: "col-sm-6" },
            right_column
        )
    );
}

/**
 * props
 * -----
 * history (Array):         list of played matches or null
 * edit_history_url (str)
 * ranking_url (str)
 * draw_service_url (str)
 * submit_url (str)
 * players_tab_url (str)
 * all_players (Array):     list of all player names
 * all_disciplines (Array): list of all discipline names
 */

var Page = function (_React$Component3) {
    _inherits(Page, _React$Component3);

    function Page(props) {
        _classCallCheck(this, Page);

        var _this8 = _possibleConstructorReturn(this, (Page.__proto__ || Object.getPrototypeOf(Page)).call(this, props));

        _this8.state = {
            history: props.history || [],
            round_to_edit: null
        };
        return _this8;
    }

    _createClass(Page, [{
        key: "addToHistory",
        value: function addToHistory(matches) {
            var new_history = this.state.history.slice(0);
            new_history.push(matches);
            this.setState({ history: new_history });
        }
    }, {
        key: "editHistoryItem",
        value: function editHistoryItem(round_number, new_data) {
            var _this9 = this;

            var old_data = this.state.history[round_number];
            this.setState({ round_to_edit: null });

            if (JSON.stringify(old_data) !== JSON.stringify(new_data)) {
                var data = {
                    old_result: old_data,
                    new_result: new_data,
                    round_number: round_number
                };
                console.log("data sent to server", data);
                $.post({
                    url: this.props.edit_history_url,
                    data: {
                        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                        data: JSON.stringify(data)
                    },
                    success: function success() {
                        show_ranking(_this9.props.ranking_url);
                        var new_history = _this9.state.history.slice(0);
                        new_history[round_number] = new_data;
                        _this9.setState({ history: new_history });
                    },
                    error: function error(result) {
                        $("#errorModalLabel").text("Editing results failed");
                        $("#errorModalText").text("Error: " + result.responseText);
                        $('#errorModal').modal('show');
                    }
                });
            } else {
                $("#errorModalLabel").text("Skipping editing results");
                $("#errorModalText").text("You have not made any changes.");
                $('#errorModal').modal('show');
            }
        }
    }, {
        key: "render",
        value: function render() {
            var _this10 = this;

            var edit_history_modal_id = "edit-history-modal";

            return React.createElement(
                React.Fragment,
                null,
                React.createElement(
                    "div",
                    { className: "row" },
                    React.createElement(
                        "div",
                        { className: "col-sm-8" },
                        React.createElement(
                            "div",
                            { className: "row" },
                            React.createElement(
                                "h1",
                                null,
                                "Draw"
                            )
                        ),
                        React.createElement(Draw, {
                            draw_service_url: this.props.draw_service_url,
                            ranking_url: this.props.ranking_url,
                            submit_url: this.props.submit_url,
                            all_players: this.props.all_players,
                            all_disciplines: this.props.all_disciplines,
                            round_number: this.state.history.length + 1,
                            add_to_history: function add_to_history(m) {
                                return _this10.addToHistory(m);
                            }
                        })
                    ),
                    React.createElement("div", { className: "col-sm-4", id: "ranking-table" })
                ),
                React.createElement(
                    "div",
                    { className: "row", style: { paddingTop: "30px", paddingBottom: "30px" } },
                    React.createElement(
                        "div",
                        { className: "col-sm-3" },
                        React.createElement(
                            "a",
                            { href: this.props.players_tab_url },
                            React.createElement(
                                "button",
                                { type: "button", className: "btn btn-info" },
                                "Back to player list"
                            )
                        )
                    )
                ),
                React.createElement(History, {
                    rounds: this.state.history,
                    edit_modal_id: edit_history_modal_id,
                    edit_round: function edit_round(i) {
                        _this10.setState({ round_to_edit: i }, function () {
                            $("#" + edit_history_modal_id).modal('show');
                        });
                    }
                }),
                React.createElement(EditResultsModal, {
                    key: check_int(this.state.round_to_edit) ? this.state.round_to_edit : -1,
                    matches: this.state.round_to_edit !== null ? this.state.history[this.state.round_to_edit] : null,
                    all_players: this.props.all_players,
                    all_disciplines: this.props.all_disciplines,
                    show_results: true,
                    modalId: edit_history_modal_id,
                    title: "Edit round " + String((check_int(this.state.round_to_edit) ? this.state.round_to_edit : -1) + 1),
                    callback: function callback(m) {
                        return _this10.editHistoryItem(_this10.state.round_to_edit, m);
                    }
                })
            );
        }
    }]);

    return Page;
}(React.Component);