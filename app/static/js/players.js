var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function showErrorModal(label, error_text) {
    $("#errorModalLabel").text(label);
    $("#errorModalText").text(error_text);
    $('#errorModal').modal('show');
}

/**
 * props
 * -----
 * modalId: unique identifier string
 * title:   modal title
 * label:   label for text input field
 * submit:  function(string)
 */

var TextInputModal = function (_React$Component) {
    _inherits(TextInputModal, _React$Component);

    function TextInputModal(props) {
        _classCallCheck(this, TextInputModal);

        var _this = _possibleConstructorReturn(this, (TextInputModal.__proto__ || Object.getPrototypeOf(TextInputModal)).call(this, props));

        _this.state = { text_input: '' };

        _this.handleChange = _this.handleChange.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }

    _createClass(TextInputModal, [{
        key: "handleChange",
        value: function handleChange(event) {
            event.preventDefault();
            this.setState({ text_input: event.target.value });
        }
    }, {
        key: "handleSubmit",
        value: function handleSubmit(event) {
            event.preventDefault();
            $('#' + this.props.modalId).modal('hide');
            this.props.submit(this.state.text_input);
            this.setState({ text_input: '' });
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            return React.createElement(
                "div",
                { className: "modal fade", id: this.props.modalId, tabIndex: "-1", role: "dialog", "aria-labelledby": this.props.modalId + "-title" },
                React.createElement(
                    "div",
                    { className: "modal-dialog modal-dialog-centered", role: "document" },
                    React.createElement(
                        "div",
                        { className: "modal-content" },
                        React.createElement(
                            "div",
                            { className: "modal-header" },
                            React.createElement(
                                "h4",
                                { className: "modal-title", id: this.props.modalId + "-title" },
                                this.props.title
                            ),
                            React.createElement(
                                "button",
                                { type: "button", className: "close", "data-dismiss": "modal", "aria-label": "Close" },
                                React.createElement(
                                    "span",
                                    { "aria-hidden": "true" },
                                    "\xD7"
                                )
                            )
                        ),
                        React.createElement(
                            "form",
                            { onSubmit: function onSubmit(event) {
                                    return _this2.handleSubmit(event);
                                } },
                            React.createElement(
                                "div",
                                { className: "modal-body" },
                                React.createElement(
                                    "label",
                                    { htmlFor: this.props.modalId + "-text-input", className: "control-label" },
                                    this.props.label
                                ),
                                React.createElement("input", { type: "text",
                                    className: "form-control",
                                    id: this.props.modalId + "-text-input",
                                    onChange: function onChange(event) {
                                        return _this2.handleChange(event);
                                    },
                                    value: this.state.text_input
                                })
                            ),
                            React.createElement(
                                "div",
                                { className: "modal-footer" },
                                React.createElement(
                                    "button",
                                    { type: "button", className: "btn btn-default", "data-dismiss": "modal" },
                                    "Cancel"
                                ),
                                React.createElement(
                                    "button",
                                    { type: "submit", className: "btn btn-primary", value: "Submit" },
                                    "+"
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return TextInputModal;
}(React.Component);

/**
 * props
 * -----
 * name:    unique discipline name
 * onClick: function to be executed when "x" is clicked
 */


function Discipline(props) {
    return React.createElement(
        "div",
        { id: props.name, className: "game-category" },
        props.name,
        React.createElement(
            "button",
            {
                type: "button",
                className: "close",
                "aria-label": "Close",
                onClick: props.onClick
            },
            React.createElement(
                "span",
                { "aria-hidden": "true" },
                "\xD7"
            )
        )
    );
}

/**
 * props
 * -----
 * inputModalId: ID of the modal that should show up when button is clicked
 * button_text:  text displayed in button
 */
function AddButton(props) {
    return React.createElement(
        "button",
        {
            type: "button",
            className: "btn btn-primary btn-lg extra-margin",
            "data-toggle": "modal",
            "data-target": "#" + props.inputModalId
        },
        props.button_text
    );
}

/**
 * props
 * -----
 * disciplines:             (Array of String) discipline names
 * add_discipline_url:      url to POST endpoint for adding disciplines
 * remove_discipline_url:   url to POST endpoint for removing disciplines
 */

var DisciplineList = function (_React$Component2) {
    _inherits(DisciplineList, _React$Component2);

    function DisciplineList(props) {
        _classCallCheck(this, DisciplineList);

        var _this3 = _possibleConstructorReturn(this, (DisciplineList.__proto__ || Object.getPrototypeOf(DisciplineList)).call(this, props));

        _this3.state = {
            disciplines: new Set(props.disciplines)
        };
        return _this3;
    }

    _createClass(DisciplineList, [{
        key: "addDiscipline",
        value: function addDiscipline(new_category) {
            var _this4 = this;

            console.log("Adding discipline", new_category);
            $.post({
                url: this.props.add_discipline_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    name: new_category
                },
                success: function success() {
                    var new_disciplines_set = _this4.state.disciplines;
                    new_disciplines_set.add(new_category);
                    _this4.setState({
                        disciplines: new_disciplines_set
                    });
                },
                error: function error(result) {
                    return showErrorModal("Adding new discipline failed", "Error: " + result.responseText);
                }
            });
        }
    }, {
        key: "removeDiscipline",
        value: function removeDiscipline(discipline) {
            var _this5 = this;

            console.assert(this.state.disciplines.has(discipline), "Bug: discipline " + String(discipline) + " not in disciplines set");
            console.log("Removing discipline", discipline);
            $.post({
                url: this.props.remove_discipline_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    name: discipline
                },
                success: function success() {
                    var new_disciplines_set = _this5.state.disciplines;
                    new_disciplines_set.delete(discipline);
                    _this5.setState({
                        disciplines: new_disciplines_set
                    });
                },
                error: function error(result) {
                    return showErrorModal("Removing discipline '" + discipline + "' failed", "Error: " + result.responseText);
                }
            });
        }
    }, {
        key: "render",
        value: function render() {
            var _this6 = this;

            var list_items = [];
            this.state.disciplines.forEach(function (discipline) {
                list_items.push(React.createElement(Discipline, {
                    key: discipline,
                    name: discipline,
                    onClick: function onClick() {
                        return _this6.removeDiscipline(discipline);
                    }
                }));
            });

            var inputModalName = "add-category-modal";
            return React.createElement(
                React.Fragment,
                null,
                list_items,
                React.createElement(
                    "div",
                    { className: "game-category" },
                    React.createElement(AddButton, {
                        inputModalId: inputModalName,
                        button_text: "+"
                    })
                ),
                React.createElement(TextInputModal, {
                    modalId: inputModalName,
                    title: "Add Category",
                    label: "category name:",
                    submit: function submit(d) {
                        return _this6.addDiscipline(d);
                    }
                })
            );
        }
    }]);

    return DisciplineList;
}(React.Component);

/**
 * props
 * -----
 * name:                player name
 * counter:             index displayed on the right
 * seed:                player seed (int >= 0)
 * handleNameChange:    handle for changes to name input
 * handleSeedChange:    handle for changes to seed input
 * removePlayer:        function that removes active player
 */


function ActivePlayer(props) {
    return React.createElement(
        "tr",
        null,
        React.createElement(
            "td",
            { className: "list-counter" },
            props.counter
        ),
        React.createElement(
            "td",
            null,
            React.createElement("input", {
                type: "text",
                value: props.name,
                className: "player-name",
                onChange: props.handleNameChange
            })
        ),
        React.createElement(
            "td",
            null,
            React.createElement("input", {
                type: "number",
                min: "0",
                value: props.seed,
                className: "player-seed",
                onChange: props.handleSeedChange
            })
        ),
        React.createElement(
            "td",
            null,
            React.createElement(
                "button",
                {
                    type: "button",
                    className: "btn-info remove-player-button",
                    onClick: props.removePlayer
                },
                "\u2718"
            )
        )
    );
}

/**
 * props
 * -----
 * players: (Array) active players to display
 *      [{
 *          key  : unique, static key
 *          name : player name
 *          seed : player seed (int > 0)
 *      }]
 * 
 * changeName: function(index, newName) handler for player name changes
 * changeSeed: function(index, newSeed) handler for player seed changes
 * removePlayer: function(index)        handler for removing player from list of active players
 */
function ActivePlayersTable(props) {

    function _handleNameChange(event, index) {
        event.preventDefault();
        props.changeName(index, event.target.value);
    }

    function _handleSeedChange(event, index) {
        event.preventDefault();
        props.changeSeed(index, event.target.value);
    }

    function handleRemovePlayer(event, index) {
        event.preventDefault();
        props.removePlayer(index);
    }

    var active_players = [];
    props.players.forEach(function (player, index) {
        active_players.push(React.createElement(ActivePlayer, {
            key: index,
            counter: index + 1,
            name: player.name,
            seed: player.seed,
            handleNameChange: function handleNameChange(event) {
                return _handleNameChange(event, index);
            },
            handleSeedChange: function handleSeedChange(event) {
                return _handleSeedChange(event, index);
            },
            removePlayer: function removePlayer(event) {
                return handleRemovePlayer(event, index);
            }
        }));
    });

    return React.createElement(
        "table",
        null,
        React.createElement(
            "thead",
            null,
            React.createElement(
                "tr",
                null,
                React.createElement("th", null),
                React.createElement(
                    "th",
                    null,
                    "Name"
                ),
                React.createElement(
                    "th",
                    null,
                    "Level"
                ),
                React.createElement("th", null)
            )
        ),
        React.createElement(
            "tbody",
            null,
            active_players
        )
    );
}

function ApplyChangesButton(props) {
    return React.createElement(
        "button",
        {
            type: "button",
            id: "edit-players-button",
            className: "btn btn-primary",
            onClick: props.onClick
        },
        props.text
    );
}

function RemovedPlayersTable(props) {
    var players = [];
    props.players.forEach(function (player, index) {
        players.push(React.createElement(
            "tr",
            { key: index },
            React.createElement(
                "td",
                null,
                player.name
            ),
            React.createElement(
                "td",
                null,
                player.seed
            )
        ));
    });
    return React.createElement(
        "table",
        null,
        React.createElement(
            "thead",
            null,
            React.createElement(
                "tr",
                null,
                React.createElement(
                    "th",
                    null,
                    "Name"
                ),
                React.createElement(
                    "th",
                    null,
                    "Level"
                )
            )
        ),
        React.createElement(
            "tbody",
            null,
            players
        )
    );
}

var Players = function (_React$Component3) {
    _inherits(Players, _React$Component3);

    function Players(props) {
        _classCallCheck(this, Players);

        var _this7 = _possibleConstructorReturn(this, (Players.__proto__ || Object.getPrototypeOf(Players)).call(this, props));

        var activePlayers = [];
        props.activePlayers.forEach(function (player) {
            activePlayers.push({
                name: player.name,
                seed: parseInt(String(player.seed)),
                original_name: player.name,
                original_seed: parseInt(String(player.seed))
            });
        });

        _this7.state = {
            activePlayers: activePlayers,
            removedPlayers: props.removedPlayers
        };
        return _this7;
    }

    _createClass(Players, [{
        key: "changeName",
        value: function changeName(index, newName) {
            this.state.activePlayers[index].name = newName;
            this.setState({
                activePlayers: this.state.activePlayers
            });
        }
    }, {
        key: "changeSeed",
        value: function changeSeed(index, newSeed) {
            this.state.activePlayers[index].seed = newSeed;
            this.setState({
                activePlayers: this.state.activePlayers
            });
        }
    }, {
        key: "addPlayer",
        value: function addPlayer(newName) {
            var _this8 = this;

            var seed = 0;
            console.log("Adding player", newName);
            $.post({
                url: this.props.add_player_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    data: JSON.stringify({ name: newName, seed: seed })
                },
                success: function success() {
                    _this8.state.activePlayers.push({
                        name: newName,
                        seed: seed,
                        original_name: newName,
                        original_seed: seed
                    });
                    _this8.setState({ activePlayers: _this8.state.activePlayers });
                },
                error: function error(result) {
                    return showErrorModal("Adding new player failed", "Error: " + result.responseText);
                }
            });
        }
    }, {
        key: "removePlayer",
        value: function removePlayer(index) {
            var _this9 = this;

            var originalName = this.state.activePlayers[index].original_name;
            var name = this.state.activePlayers[index].name.length ? this.state.activePlayers[index].name : originalName;
            $("#errorModal").find(".modal-footer").find("button").off().on('click', function (event) {
                $("#errorModal").find(".modal-footer").find("button").off();
                console.log("Removing player", originalName);
                event.preventDefault();
                $.post({
                    url: _this9.props.remove_player_url,
                    data: {
                        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                        name: originalName
                    },
                    success: function success() {
                        _this9.state.removedPlayers.push({
                            name: originalName,
                            seed: _this9.state.activePlayers[index].original_seed
                        });
                        _this9.state.activePlayers.splice(index, 1);
                        _this9.setState({
                            activePlayers: _this9.state.activePlayers,
                            removedPlayers: _this9.state.removedPlayers
                        });
                    },
                    error: function error(result) {
                        return showErrorModal("Removing player failed", "Error: " + result.responseText);
                    }
                });
            });
            showErrorModal("This action cannot be reversed!", "Are you sure you want to remove player '" + name + "'?");
        }
    }, {
        key: "editPlayers",
        value: function editPlayers(changes) {
            var _this10 = this;

            changes.forEach(function (index) {
                var player = _this10.state.activePlayers[index];
                $.post({
                    url: _this10.props.edit_player_url,
                    data: {
                        csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                        data: JSON.stringify({
                            old_name: player.original_name,
                            new_name: player.name,
                            new_seed: player.seed
                        })
                    },
                    success: function success() {
                        Object.assign(_this10.state.activePlayers[index], {
                            original_name: player.name,
                            original_seed: player.seed
                        });
                        _this10.setState({ activePlayers: _this10.state.activePlayers });
                    },
                    error: function error(result) {
                        Object.assign(_this10.state.activePlayers[index], {
                            name: player.original_name,
                            seed: player.original_seed
                        });
                        _this10.setState({ activePlayers: _this10.state.activePlayers });

                        $("#errorModalLabel").text("Editing player data failed");
                        $("#errorModalText").text("Error: " + result.responseText);
                        $('#errorModal').modal('show');
                    }
                });
            });
        }
    }, {
        key: "gatherChanges",
        value: function gatherChanges() {
            var changes = [];
            this.state.activePlayers.forEach(function (player, index) {
                if (player.original_name !== player.name || player.original_seed !== player.seed) {
                    changes.push(index);
                }
            });
            return changes;
        }
    }, {
        key: "render",
        value: function render() {
            var _this11 = this;

            var changes = this.gatherChanges();
            var inputModalName = "add-player-modal";
            var removedTable = this.state.removedPlayers.length ? React.createElement(
                "div",
                { className: "col-sm-5" },
                React.createElement(
                    "div",
                    { className: "row" },
                    React.createElement(
                        "h4",
                        null,
                        "Removed from tournament"
                    )
                ),
                React.createElement(
                    "div",
                    { className: "row" },
                    React.createElement(RemovedPlayersTable, { players: this.state.removedPlayers })
                )
            ) : "";

            return React.createElement(
                React.Fragment,
                null,
                React.createElement(
                    "div",
                    { className: "col-sm-7" },
                    React.createElement(
                        "div",
                        { className: "row" },
                        React.createElement(
                            "h4",
                            null,
                            "Players"
                        )
                    ),
                    React.createElement(
                        "div",
                        { className: "row" },
                        React.createElement(ActivePlayersTable, {
                            players: this.state.activePlayers,
                            changeName: function changeName(index, newName) {
                                return _this11.changeName(index, newName);
                            },
                            changeSeed: function changeSeed(index, newSeed) {
                                return _this11.changeSeed(index, newSeed);
                            },
                            removePlayer: function removePlayer(index) {
                                return _this11.removePlayer(index);
                            }
                        })
                    ),
                    React.createElement(
                        "div",
                        { className: "row" },
                        changes.length ? React.createElement(ApplyChangesButton, {
                            onClick: function onClick() {
                                return _this11.editPlayers(changes);
                            },
                            text: "Apply Changes"
                        }) : "",
                        React.createElement(AddButton, {
                            inputModalId: inputModalName,
                            button_text: "+"
                        })
                    )
                ),
                removedTable,
                React.createElement(TextInputModal, {
                    modalId: inputModalName,
                    title: "Add Player",
                    label: "player name:",
                    submit: function submit(p) {
                        return _this11.addPlayer(p);
                    }
                })
            );
        }
    }]);

    return Players;
}(React.Component);