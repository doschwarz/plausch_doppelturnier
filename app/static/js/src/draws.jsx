function showErrorModal(label, error_text) {
    $("#errorModalLabel").text(label);
    $("#errorModalText").text(error_text);
    $('#errorModal').modal('show');
}

function show_ranking(url) {
    $.get({
        url: url,
        success: (result) => {
            $("#ranking-table").html(result);
        },
        error: (result) => {
            showErrorModal("Calculating ranking failed", "Error: " + result.responseText);
        }
    });
}

function check_int(intString) {
    return String(parseInt(String(intString))) === String(intString)
}

function parse_result(resultString) {
    const digits = resultString.replace(/\s+/g, '').split("-");
    if (digits.length !== 2) {
        return [digits, false];
    }
    if (check_int(digits[0]) && check_int(digits[1])) {
        const d0 = parseInt(digits[0]);
        const d1 = parseInt(digits[1]);
        return [[d0, d1], d0 !== d1];
    }
    return [digits, false];
}

/**
 * props
 * -----
 * value:               displayed value
 * mutable (bool):      make a <select> or just return value
 * all_values (Array):  all discipline names
 * onChange (function): handle for changes of <select> value
 */
function Select(props) {
    if (props.mutable) {
        const options = props.all_values.map((value, index) => {
            return <option
                key={index}
                value={value}
            >{value}</option>;
        });
    
        return <select
                    onChange={props.onChange}
                    value={props.value}
                >{options}</select>;
    }
    else {
        return props.value;
    }
}

/**
 * props
 * -----
 * discipline (str):            discipline of match or null, then discipline column will not be displayed
 * mutable_discipline (bool)
 * all_disciplines (Array):     all disciplines
 * changeDiscipline (function): (newDiscipline) -> void
 * 
 * players (Object):            player names; keys : {t0p0, t0p1, t1p0, t1p1} or {s0, s1}
 * mutable_players (bool)
 * all_players (Array):         all players including " - "
 *                              in the order they should appear in the <select> tags
 * changeName (function):       (playerKey, newName) -> void
 * 
 * result (str):                result or null, then result column will not be displayed
 * mutable_result (bool)
 * changeResult (function):     (newResult) -> void
 */
function Match(props) {

    function handleDisciplineChange(event) {
        event.preventDefault();
        props.changeDiscipline(event.target.value);
    }

    function handleNameChange (event, playerKey) {
        event.preventDefault();
        props.changeName(playerKey, event.target.value);
    }

    function handleResultChange (event) {
        event.preventDefault();
        props.changeResult(event.target.value);
    }

    let disciplineCol = null;
    if (props.discipline) {
        disciplineCol = <div className="col-sm-2"><p>
            <Select
                value={props.discipline}
                mutable={props.mutable_discipline}
                all_values={props.all_disciplines}
                onChange={(event) => handleDisciplineChange(event)}
            />:
        </p></div>;
    }

    let players = {};
    for (const [playerKey, name] of Object.entries(props.players)) {
        players[playerKey] = <Select
                                value={name}
                                mutable={props.mutable_players}
                                all_values={props.all_players}
                                onChange={(event) => handleNameChange(event, playerKey)}
                             />;
    }

    const team0 = players.s0 || <React.Fragment>{players.t0p0} + {players.t0p1}</React.Fragment>;
    const team1 = players.s1 || <React.Fragment>{players.t1p0} + {players.t1p1}</React.Fragment>;

    let resultCol = null;
    if (props.result) {
        if (props.mutable_result) {
            resultCol = <div className='col-sm-2'><input
                        type='text'
                        value={props.result}
                        className='doubles-input'
                        onChange={(event) => handleResultChange(event)}
                     /></div>;
        }
        else {
            resultCol = <div className='col-sm-2'><p>{props.result}</p></div>;
        }
    }

    const col_width = String(4 + (disciplineCol === null) + (resultCol === null));
    
    return (
        <div className='row'>
            {disciplineCol}
            <div className={'col-sm-' + col_width}>
                <p>{team0}<span style={{float: 'right'}}> vs. </span></p>
            </div>
            <div className={'col-sm-' + col_width}>
                <p>{team1}</p>
            </div>
            {resultCol}
        </div>
    );
}

/**
 * props
 * -----
 * matches (Object):        {doubles: [{players: {t0p0: (str), t0p1: (str), t1p0: (str), t1p1: (str)},
 *                                      result: (str),
 *                                      discipline: (str) or null },
 *                                      ... ],
 *                           singles: {players: {s0: (str), s1: (str)},
 *                                     result: (str),
 *                                     discipline: (str) or null },
 *                           bye: (str) }
 * 
 * mutable_players (bool)
 * all_players (Array):     (str) all player names
 * changeName (function):   (index, playerKey, newName) -> void
 * 
 * mutable_disciplines (bool)
 * all_disciplines (Array):     (str) all discipline names
 * changeDiscipline (function): (index, newDiscipline) -> void 
 * 
 * show_results (bool)
 * mutable_results (bool)
 * changeResult (function):     (index, newResult)
 * 
 * %where (index) is {0, 1, ...} for doubles matches, 'singles' or 'bye'
 */
function MatchesList(props) {

    const doubles_rows = props.matches.doubles.map((match, index) => {
        return <Match
            key={index}
            discipline={match.discipline}
            mutable_discipline={props.mutable_disciplines}
            all_disciplines={props.all_disciplines}
            changeDiscipline={(d) => props.changeDiscipline(index, d)}

            players={match.players}
            mutable_players={props.mutable_players}
            all_players={props.all_players}
            changeName={(k, n) => props.changeName(index, k, n)}

            result={props.show_results ? (match.result || " - ") : null}
            mutable_result={props.mutable_results}
            changeResult={(r) => props.changeResult(index, r)}
        />;
    });

    let singles_rows = null;
    if ('singles' in props.matches) {
        const match = props.matches.singles;
        singles_rows = <React.Fragment>
            <div className="row">
                <div className="col-sm-2">
                    <p><b>Singles:</b></p>
                </div>
            </div>
            <Match
                discipline={match.discipline}
                mutable_discipline={props.mutable_disciplines}
                all_disciplines={props.all_disciplines}
                changeDiscipline={(d) => props.changeDiscipline("singles", d)}

                players={match.players}
                mutable_players={props.mutable_players}
                all_players={props.all_players}
                changeName={(k, n) => props.changeName("singles", k, n)}

                result={props.show_results ? (match.result || " - ") : null}
                mutable_result={props.mutable_results}
                changeResult={(r) => props.changeResult("singles", r)}
            />
        </React.Fragment>;
    }

    const bye_row = !('bye' in props.matches)
        ? null
        : <div className="row"><div className="col-sm-12">
            <p>
                <b>Bye: </b>
                <Select
                    value={props.matches.bye}
                    mutable={props.mutable_players}
                    all_values={props.all_players}
                    onChange={(event) => {
                        event.preventDefault();
                        props.changeName("bye", null, event.target.value);
                    }}
                />
            </p>
        </div></div>;

    return (<React.Fragment>
        <div className="row">
            <div className="col-sm-2">
                <p><b>Doubles:</b></p>
            </div>
        </div>
        {doubles_rows}
        {singles_rows}
        {bye_row}
    </React.Fragment>);
}

/**
 * props
 * -----
 * draw_service_url (str)
 * ranking_url (str)
 * submit_url (str)
 * round_number (int)
 * all_players (Array):         list of all player names
 * all_disciplines (Array):     list of all discipline names
 * add_to_history (function):   (match) -> void
 */
class Draw extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            matches: null,
            round_number: this.props.round_number || 1,
            data_ok: null,
            show_modify_draw_modal: false
        };
    }

    changeResult(index, newResult) {
        const data = parse_result(newResult);
        let new_matches = JSON.parse(JSON.stringify(this.state.matches));
        if (data[1]) {
            newResult = String(data[0][0]) + " - " + String(data[0][1]);
        }

        if (index === "singles") {
            new_matches.singles['result'] = newResult;
        }
        else {
            // TODO: assert index is number in [0, array.length]
            new_matches.doubles[index]['result'] = newResult;
        }
        let tmp = {};
        tmp[index] = data[1];
        this.setState({
            matches: new_matches,
            data_ok: Object.assign(this.state.data_ok, tmp)
        });
    }

    requestDraw(repeatDraw=false) {
        $.get({
            url: this.props.draw_service_url + (repeatDraw ? "?repeat" : ""),
            success: (result) => {
                console.log("match received from server", result);
                let data_ok = {};
                if ('singles' in result) {
                    data_ok['singles'] = false;
                }
                result.doubles.forEach((val, index) => {
                    data_ok[index] = false;
                });
                this.setState({matches: result, data_ok: data_ok});
            },
            error: (result) => {
                $("#errorModalLabel").text("Making draw failed");
                $("#errorModalText").text("Error: " + result.responseText);
                $('#errorModal').modal('show');
            }
        });
    }

    submitResults(event) {
        event.preventDefault();
        let wrong_data = null;
        for (const [key, data_ok] of Object.entries(this.state.data_ok)) {
            if (!data_ok) {
                wrong_data = check_int(key) ? (String(parseInt(key) + 1) + ". doubles") : key;
                break;
            }
        }
        if (wrong_data) {
            $("#errorModalLabel").text("Error: Incorrect results");
            $("#errorModalText").text("Result of " + wrong_data + " not entered correctly");
            $('#errorModal').modal('show');
        }
        else {
            console.log("results sent to server", this.state.matches);
            $.post({
                url: this.props.submit_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    data: JSON.stringify(this.state.matches)
                },
                success: () => {
                    console.log("Successfully submitted result");
                    this.props.add_to_history(this.state.matches);
                    this.setState({
                        round_number: this.state.round_number + 1,
                        matches: null,
                        data_ok: null
                    });
                    show_ranking(this.props.ranking_url);
                    this.requestDraw();
                },
                error: (result) => {
                    $("#errorModalLabel").text("Submitting results failed");
                    $("#errorModalText").text("Error: " + result.responseText);
                    $('#errorModal').modal('show');
                }
            });
        }
    }

    render () {
        const matchIsNull = this.state.matches ? false : true;
        const modal_id = "change-current-draw-modal";

        const drawRows = matchIsNull ? null : <React.Fragment>
            <div className="row">
                <div className="col-sm-5">
                <h3>Round {this.state.round_number}</h3>
                </div>
                <div className="col-sm-2">
                    <button
                        type="button"
                        className="btn btn-outline-secondary"
                        onClick={(event) => {
                            event.preventDefault();
                            this.setState({show_modify_draw_modal: true}, () => {
                                $("#" + modal_id).modal('show')
                            });
                        }}
                    >&#x270E;</button>
                </div>
            </div>
            <form onSubmit={(event) => this.submitResults(event)}>
                <MatchesList
                    matches={this.state.matches}
                    mutable_players={false}
                    mutable_disciplines={false}
                    show_results={true}
                    mutable_results={true}
                    changeResult={(i, r) => this.changeResult(i, r)}
                />
                <div className="row">
                    <button
                        type="submit"
                        className="btn btn-info"
                    >
                        Submit results
                    </button>
                </div>
            </form>
        </React.Fragment>;

        return (<React.Fragment>
            <div className="row" style={{paddingBottom: "10px"}}>
                <button
                    type="button"
                    className="btn btn-info"
                    onClick={(event) => {
                        event.preventDefault();
                        this.requestDraw(!matchIsNull)
                    }}
                >
                    {(matchIsNull) ? "Draw next round" : "Repeat draw"}
                </button>
            </div>
            {drawRows}
            <EditResultsModal
                key={this.state.show_modify_draw_modal || 0}
                matches={this.state.show_modify_draw_modal ? this.state.matches : null}
                all_players={this.props.all_players}
                all_disciplines={this.props.all_disciplines}
                show_results={false}
                modalId={modal_id}
                title="Change current draw"
                callback={(matches) => {
                    this.setState({matches: matches});
                }}
            />
        </React.Fragment>);
    }
}

function initDataOk(matches) {
    let data_ok = {};
    if ('bye' in matches) {
        data_ok['bye'] = {
            name: true
        };
    }
    if ('singles' in matches) {
        data_ok['singles'] = {
            result: true,
            s0: true,
            s1: true,
        };
    }
    matches.doubles.forEach((val, index) => {
        data_ok[index] = {
            result: true,
            t0p0: true,
            t0p1: true,
            t1p0: true,
            t1p1: true
        }
    });

    return data_ok;
}

/**
 * props
 * -----
 * matches (Object):        matches object or null
 * all_players (Array):     list of all player names
 * all_disciplines (Array): list of all discipline names
 * show_results (bool):     also make results editable (not displayed otherwise)
 * modalId (str):           id of modal
 * title (str):             displayed modal title
 * callback (function):     (matches) -> void
 */
class EditResultsModal extends React.Component {
    constructor(props) {
        super(props);
        let all_players = props.all_players.slice(0);
        all_players.unshift("-");
        this.state = {
            matches: props.matches ? JSON.parse(JSON.stringify(props.matches)) : null,
            all_players: all_players,
            data_ok: props.matches ? initDataOk(props.matches) : null
        };
    }

    changeResult(index, newResult) {
        const data = parse_result(newResult);
        if (data[1]) {
            newResult = String(data[0][0]) + " - " + String(data[0][1]);
        }
        
        let new_matches = JSON.parse(JSON.stringify(this.state.matches));
        if (index === "singles") {
            new_matches.singles.result = newResult;
        }
        else {
            // TODO: assert index is number in [0, array.length]
            new_matches.doubles[index].result = newResult;
        }

        let new_data_ok = JSON.parse(JSON.stringify(this.state.data_ok));
        new_data_ok[index].result = data[1];
        this.setState({
            matches: new_matches,
            data_ok: new_data_ok
        });
    }

    changeName(index, playerKey, newName) {
        // TODO: assert newName in this.props.all_players
        const new_name_not_dash = newName !== "-";

        let new_matches = JSON.parse(JSON.stringify(this.state.matches));
        let new_data_ok = JSON.parse(JSON.stringify(this.state.data_ok));
        let new_all_players = this.state.all_players.slice(0);

        let oldName;
        if (index === "bye") {
            oldName = this.state.matches.bye;
            new_matches.bye = newName;
            new_data_ok.bye.name = new_name_not_dash;
        }
        else if (index === "singles") {
            oldName = this.state.matches.singles.players[playerKey];
            new_matches.singles.players[playerKey] = newName;
            new_data_ok.singles[playerKey] = new_name_not_dash;
        }
        else {
            oldName = this.state.matches.doubles[index].players[playerKey];
            new_matches.doubles[index].players[playerKey] = newName;
            new_data_ok[index][playerKey] = new_name_not_dash;
        }

        const old_name_not_dash = oldName !== "-";

        if (new_name_not_dash) {
            this.state.matches.doubles.forEach((match, i) => {
                for (const [k, name] of Object.entries(match.players)) {
                    if (name === newName && (i !== index || k !== playerKey)) {
                        new_matches.doubles[i].players[k] = "-";
                        new_data_ok[i][k] = false;
                        if (old_name_not_dash) {
                            new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                            new_all_players.unshift(oldName);
                        }
                    }
                }
            });

            for (const [k, name] of Object.entries(this.state.matches.singles.players)) {
                if (name === newName && k !== playerKey) {
                    new_matches.singles.players[k] = "-";
                    new_data_ok.singles[k] = false;
                    if (old_name_not_dash) {
                        new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                        new_all_players.unshift(oldName);
                    }
                }
            }

            if (this.state.matches.bye === newName && index !== "bye") {
                new_matches.bye = "-";
                new_data_ok.bye.name = false;
                if (old_name_not_dash) {
                    new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
                    new_all_players.unshift(oldName);
                }
            }
            
            if (oldName === "-") {
                new_all_players.splice(this.state.all_players.indexOf(newName), 1);
                new_all_players.push(newName);
            }
        } else if (old_name_not_dash) {
            new_all_players.splice(this.state.all_players.indexOf(oldName), 1);
            new_all_players.unshift(oldName);
        }

        this.setState({
            matches: new_matches,
            data_ok: new_data_ok,
            all_players: new_all_players
        });
    }

    changeDiscipline(index, newDiscipline) {
        // TODO: assert discipline is in all_disciplines
        let new_matches = JSON.parse(JSON.stringify(this.state.matches));
        if (index === "singles") {
            new_matches.singles.discipline = newDiscipline;
        }
        else {
            // TODO: assert index is number in [0, array.length]
            new_matches.doubles[index].discipline = newDiscipline;
        }
        this.setState({matches: new_matches});
    }

    applyChanges (event) {
        event.preventDefault();
        let wrong_data = null;
        for (const [key, data] of Object.entries(this.state.data_ok)) {
            for (const [key2, data_ok] of Object.entries(data)) {
                if (!data_ok) {
                    wrong_data = (key2 === "result" ? "Result of " : "Name in ") + (check_int(key) ? (String(parseInt(key) + 1) + ". doubles") : key);
                    break;
                }
            }
            if (wrong_data) {
                break;
            }
        }
        if (wrong_data) {
            $("#errorModalLabel").text("Error: Incorrect results");
            $("#errorModalText").text(wrong_data + " is invalid.");
            $('#errorModal').modal('show');
        }
        else {
            $('#' + this.props.modalId).modal('hide');
            this.props.callback(this.state.matches);
        }
    }

    render () {
        const matchesList = this.state.matches
            ? <MatchesList
                matches={this.state.matches}
                mutable_players={true}
                all_players={this.state.all_players}
                changeName={(i, k, n) => this.changeName(i, k, n)}

                mutable_disciplines={true}
                all_disciplines={this.props.all_disciplines}
                changeDiscipline={(i, d) => this.changeDiscipline(i, d)}

                show_results={this.props.show_results}
                mutable_results={true}
                changeResult={(i, r) => this.changeResult(i, r)}
            />
            : null;
        return (
            <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog" aria-labelledby={this.props.modalId + "-title"}>
                <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title" id={this.props.modalId + "-title"}>
                                {this.props.title}
                            </h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={(event) => this.applyChanges(event)}>
                            <div className="modal-body">
                                {matchesList}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" className="btn btn-primary">Confirm</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * props
 * -----
 * rounds (Array):          list of results (matches Objects)
 * edit_modal_id (str)      
 * edit_round (function):   (int) -> void
 */
function History(props) {
    let left_column = [];
    let right_column = [];

    function editRound(event, index) {
        event.preventDefault();
        props.edit_round(index);
    }

    props.rounds.forEach((round, index) => {
        const item = <div className="row" key={index}><div className="col-sm-12">
            <div className="row">
                <div className="col-sm-3">
                    <h3 className="history-round-number">Round {index + 1}</h3>
                </div>
                <div className="col-sm-1">
                    <button
                        type="button"
                        className="btn btn-outline-secondary"
                        onClick={(event) => editRound(event, index)}
                    >&#x270E;</button>
                </div>
            </div>
            <MatchesList
                matches={round}
                mutable_players={false}
                mutable_disciplines={false}
                show_results={true}
                mutable_results={false}
            />
        </div></div>;
        if (index % 2) {
            right_column.push(item);
        }
        else {
            left_column.push(item);
        }
    });

    return (<div className="row">
        <div className="col-sm-6">
            {left_column}
        </div>
        <div className="col-sm-6">
            {right_column}
        </div>
    </div>);
}

/**
 * props
 * -----
 * history (Array):         list of played matches or null
 * edit_history_url (str)
 * ranking_url (str)
 * draw_service_url (str)
 * submit_url (str)
 * players_tab_url (str)
 * all_players (Array):     list of all player names
 * all_disciplines (Array): list of all discipline names
 */
class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: props.history || [],
            round_to_edit: null
        }
    }

    addToHistory(matches) {
        let new_history = this.state.history.slice(0);
        new_history.push(matches);
        this.setState({history: new_history});
    }

    editHistoryItem(round_number, new_data) {
        const old_data = this.state.history[round_number];
        this.setState({round_to_edit: null});

        if (JSON.stringify(old_data) !== JSON.stringify(new_data)) {
            const data = {
                old_result: old_data,
                new_result: new_data,
                round_number: round_number
            }
            console.log("data sent to server", data);
            $.post({
                url: this.props.edit_history_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    data: JSON.stringify(data)
                },
                success: () => {
                    show_ranking(this.props.ranking_url);
                    let new_history = this.state.history.slice(0);
                    new_history[round_number] = new_data;
                    this.setState({history: new_history});
                },
                error: (result) => {
                    $("#errorModalLabel").text("Editing results failed");
                    $("#errorModalText").text("Error: " + result.responseText);
                    $('#errorModal').modal('show');
                }
            });
        } else {
            $("#errorModalLabel").text("Skipping editing results");
            $("#errorModalText").text("You have not made any changes.");
            $('#errorModal').modal('show');
        }
    }

    render () {
        const edit_history_modal_id = "edit-history-modal";

        return (<React.Fragment>
            <div className="row">
                <div className="col-sm-8">
                    <div className="row">
                        <h1>Draw</h1>
                    </div>
                    <Draw
                        draw_service_url={this.props.draw_service_url}
                        ranking_url={this.props.ranking_url}
                        submit_url={this.props.submit_url}
                        all_players={this.props.all_players}
                        all_disciplines={this.props.all_disciplines}
                        round_number={this.state.history.length + 1}
                        add_to_history={(m) => this.addToHistory(m)}
                    />
                </div>
                <div className="col-sm-4" id="ranking-table"></div>
            </div>
            <div className="row" style={{paddingTop: "30px", paddingBottom: "30px"}}>
                <div className="col-sm-3">
                    <a href={this.props.players_tab_url}>
                        <button type="button" className="btn btn-info">Back to player list</button>
                    </a>
                </div>
            </div>
            <History
                rounds={this.state.history}
                edit_modal_id={edit_history_modal_id}
                edit_round={(i) => {
                    this.setState({round_to_edit: i}, () => {$("#" + edit_history_modal_id).modal('show')});
                }}
            />
            <EditResultsModal
                key={check_int(this.state.round_to_edit) ? this.state.round_to_edit : -1}
                matches={(this.state.round_to_edit !== null) ? this.state.history[this.state.round_to_edit] : null}
                all_players={this.props.all_players}
                all_disciplines={this.props.all_disciplines}
                show_results={true}
                modalId={edit_history_modal_id}
                title={"Edit round " + String((check_int(this.state.round_to_edit) ? this.state.round_to_edit : -1) + 1)}
                callback={(m) => this.editHistoryItem(this.state.round_to_edit, m)} 
            />
        </React.Fragment>);
    }
}
