function showErrorModal(label, error_text) {
    $("#errorModalLabel").text(label);
    $("#errorModalText").text(error_text);
    $('#errorModal').modal('show');
}

/**
 * props
 * -----
 * modalId: unique identifier string
 * title:   modal title
 * label:   label for text input field
 * submit:  function(string)
 */
class TextInputModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {text_input: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        event.preventDefault();
        this.setState({text_input: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        $('#' + this.props.modalId).modal('hide');
        this.props.submit(this.state.text_input);
        this.setState({text_input: ''});
    }

    render () {
        return (
            <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog" aria-labelledby={this.props.modalId + "-title"}>
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title" id={this.props.modalId + "-title"}>{this.props.title}</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form onSubmit={(event) => this.handleSubmit(event)}>
                            <div className="modal-body">
                                <label htmlFor={this.props.modalId + "-text-input"} className="control-label">{this.props.label}</label>
                                <input type="text"
                                    className="form-control"
                                    id={this.props.modalId + "-text-input"}
                                    onChange={(event) => this.handleChange(event)}
                                    value={this.state.text_input}
                                />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" className="btn btn-primary" value="Submit">+</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * props
 * -----
 * name:    unique discipline name
 * onClick: function to be executed when "x" is clicked
 */
function Discipline(props) {
    return (
        <div id={props.name} className="game-category">
            {props.name}
            <button
                type="button"
                className="close"
                aria-label="Close"
                onClick={props.onClick}
            >
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    );
}

/**
 * props
 * -----
 * inputModalId: ID of the modal that should show up when button is clicked
 * button_text:  text displayed in button
 */
function AddButton(props) {
    return (
        <button
            type="button"
            className="btn btn-primary btn-lg extra-margin"
            data-toggle="modal"
            data-target={"#" + props.inputModalId}
        >{props.button_text}</button>
    );
}

/**
 * props
 * -----
 * disciplines:             (Array of String) discipline names
 * add_discipline_url:      url to POST endpoint for adding disciplines
 * remove_discipline_url:   url to POST endpoint for removing disciplines
 */
class DisciplineList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            disciplines: new Set(props.disciplines)
        }
    }

    addDiscipline(new_category) {
        console.log("Adding discipline", new_category);
        $.post({
            url: this.props.add_discipline_url,
            data: {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                name: new_category
            },
            success: () => {
                let new_disciplines_set = this.state.disciplines;
                new_disciplines_set.add(new_category);
                this.setState({
                    disciplines: new_disciplines_set
                });
            },
            error: (result) => showErrorModal("Adding new discipline failed", "Error: " + result.responseText)
        });
    }
    
    removeDiscipline(discipline) {
        console.assert(
            this.state.disciplines.has(discipline),
            "Bug: discipline " + String(discipline) + " not in disciplines set"
        );
        console.log("Removing discipline", discipline);
        $.post({
            url: this.props.remove_discipline_url,
            data: {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                name: discipline
            },
            success: () => {
                let new_disciplines_set = this.state.disciplines;
                new_disciplines_set.delete(discipline);
                this.setState({
                    disciplines: new_disciplines_set
                });
            },
            error: (result) => showErrorModal("Removing discipline '" + discipline + "' failed", "Error: " + result.responseText)
        });
    }

    render() {
        let list_items = [];
        this.state.disciplines.forEach((discipline) => {
            list_items.push(
                <Discipline
                    key={discipline}
                    name={discipline}
                    onClick={() => this.removeDiscipline(discipline)}
                />
            );
        });
        
        const inputModalName = "add-category-modal";
        return (
            <React.Fragment>
                {list_items}
                <div className="game-category">
                    <AddButton
                        inputModalId={inputModalName}
                        button_text="+"
                    />
                </div>
                <TextInputModal
                    modalId={inputModalName}
                    title="Add Category"
                    label="category name:"
                    submit={(d) => this.addDiscipline(d)}
                />
            </React.Fragment>
        );
    }
}

/**
 * props
 * -----
 * name:                player name
 * counter:             index displayed on the right
 * seed:                player seed (int >= 0)
 * handleNameChange:    handle for changes to name input
 * handleSeedChange:    handle for changes to seed input
 * removePlayer:        function that removes active player
 */
function ActivePlayer(props) {
    return (
        <tr>
            <td className="list-counter">{props.counter}</td>
            <td>
                <input
                    type="text"
                    value={props.name}
                    className="player-name"
                    onChange={props.handleNameChange}
                />
            </td>
            <td>
                <input
                    type="number"
                    min="0"
                    value={props.seed}
                    className="player-seed"
                    onChange={props.handleSeedChange}
                />
            </td>
            <td>
                <button
                    type="button"
                    className="btn-info remove-player-button"
                    onClick={props.removePlayer}
                >
                    &#10008;
                </button>
            </td>
        </tr>
    );
}

/**
 * props
 * -----
 * players: (Array) active players to display
 *      [{
 *          key  : unique, static key
 *          name : player name
 *          seed : player seed (int > 0)
 *      }]
 * 
 * changeName: function(index, newName) handler for player name changes
 * changeSeed: function(index, newSeed) handler for player seed changes
 * removePlayer: function(index)        handler for removing player from list of active players
 */
function ActivePlayersTable(props) {

    function handleNameChange(event, index) {
        event.preventDefault();
        props.changeName(index, event.target.value);
    }

    function handleSeedChange(event, index) {
        event.preventDefault();
        props.changeSeed(index, event.target.value)
    }

    function handleRemovePlayer(event, index) {
        event.preventDefault();
        props.removePlayer(index);
    }

    let active_players = [];
    props.players.forEach((player, index) => {
        active_players.push(
            <ActivePlayer
                key={index}
                counter={index+1}
                name={player.name}
                seed={player.seed}
                handleNameChange={(event) => handleNameChange(event, index)}
                handleSeedChange={(event) => handleSeedChange(event, index)}
                removePlayer={(event) => handleRemovePlayer(event, index)}
            />
        );
    });

    return (
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Level</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {active_players}
            </tbody>
        </table>
    );
}

function ApplyChangesButton(props) {
    return (
        <button
            type="button"
            id="edit-players-button"
            className="btn btn-primary"
            onClick={props.onClick}
        >
            {props.text}
        </button>
    )
}

function RemovedPlayersTable(props) {
    let players = [];
    props.players.forEach((player, index) => {
        players.push(
            <tr key={index}>
                <td>{player.name}</td>
                <td>{player.seed}</td>
            </tr>
        );
    });
    return (
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Level</th>
                </tr>
            </thead>
            <tbody>
                {players}
            </tbody>
        </table>
    );
}

class Players extends React.Component {
    constructor(props) {
        super(props);

        let activePlayers = [];
        props.activePlayers.forEach((player) => {
            activePlayers.push({
                name: player.name,
                seed: parseInt(String(player.seed)),
                original_name: player.name,
                original_seed: parseInt(String(player.seed))
            });
        });

        this.state = {
            activePlayers: activePlayers,
            removedPlayers: props.removedPlayers
        };
    }

    changeName(index, newName) {
        this.state.activePlayers[index].name = newName;
        this.setState({
            activePlayers: this.state.activePlayers
        });
    }

    changeSeed(index, newSeed) {
        this.state.activePlayers[index].seed = newSeed;
        this.setState({
            activePlayers: this.state.activePlayers
        });
    }

    addPlayer(newName) {
        const seed = 0;
        console.log("Adding player", newName);
        $.post({
            url: this.props.add_player_url,
            data: {
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                data: JSON.stringify({name: newName, seed: seed}),
            },
            success: () => {
                this.state.activePlayers.push({
                    name: newName,
                    seed: seed,
                    original_name: newName,
                    original_seed: seed
                });
                this.setState({activePlayers: this.state.activePlayers});
            },
            error: (result) => showErrorModal("Adding new player failed", "Error: " + result.responseText)
        });
    }

    removePlayer(index) {
        const originalName = this.state.activePlayers[index].original_name;
        const name = this.state.activePlayers[index].name.length ? this.state.activePlayers[index].name : originalName;
        $("#errorModal").find(".modal-footer").find("button").off().on('click', (event) => {
            $("#errorModal").find(".modal-footer").find("button").off();
            console.log("Removing player", originalName);
            event.preventDefault();
            $.post({
                url: this.props.remove_player_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    name: originalName,
                },
                success: () => {
                    this.state.removedPlayers.push({
                        name: originalName,
                        seed: this.state.activePlayers[index].original_seed
                    })
                    this.state.activePlayers.splice(index, 1);
                    this.setState({
                        activePlayers: this.state.activePlayers,
                        removedPlayers: this.state.removedPlayers
                    });
                },
                error: (result) => showErrorModal("Removing player failed", "Error: " + result.responseText)
            });
        });
        showErrorModal("This action cannot be reversed!", "Are you sure you want to remove player '" + name + "'?");
    }

    editPlayers(changes) {
        changes.forEach((index) => {
            const player = this.state.activePlayers[index];
            $.post({
                url: this.props.edit_player_url,
                data: {
                    csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val(),
                    data: JSON.stringify({
                        old_name: player.original_name,
                        new_name: player.name,
                        new_seed: player.seed
                    })
                },
                success: () => {
                    Object.assign(this.state.activePlayers[index], {
                        original_name: player.name,
                        original_seed: player.seed
                    });
                    this.setState({activePlayers: this.state.activePlayers});
                },
                error: (result) => {
                    Object.assign(this.state.activePlayers[index], {
                        name: player.original_name,
                        seed: player.original_seed
                    });
                    this.setState({activePlayers: this.state.activePlayers});
                    
                    $("#errorModalLabel").text("Editing player data failed");
                    $("#errorModalText").text("Error: " + result.responseText);
                    $('#errorModal').modal('show');
                }
            });
        });
    }

    gatherChanges() {
        let changes = [];
        this.state.activePlayers.forEach((player, index) => {
            if (player.original_name !== player.name || player.original_seed !== player.seed) {
                changes.push(index);
            }
        });
        return changes;
    }

    render () {
        const changes = this.gatherChanges();
        const inputModalName = "add-player-modal";
        const removedTable = (this.state.removedPlayers.length)
            ? <div className="col-sm-5">
                <div className="row">
                    <h4>Removed from tournament</h4>
                </div>
                <div className="row">
                    <RemovedPlayersTable players={this.state.removedPlayers} />
                </div>
            </div>
            : "";

        return (<React.Fragment>
            <div className="col-sm-7">
                <div className="row">
                    <h4>Players</h4>
                </div>
                <div className="row">
                    <ActivePlayersTable
                        players={this.state.activePlayers}
                        changeName={(index, newName) => this.changeName(index, newName)}
                        changeSeed={(index, newSeed) => this.changeSeed(index, newSeed)}
                        removePlayer={(index) => this.removePlayer(index)}
                    />
                </div>
                <div className="row">
                    {(changes.length)
                        ? <ApplyChangesButton
                            onClick={() => this.editPlayers(changes)}
                            text="Apply Changes"
                        />
                        : ""}
                    <AddButton
                        inputModalId={inputModalName}
                        button_text="+"
                    />
                </div>
            </div>
            {removedTable}
            <TextInputModal
                modalId={inputModalName}
                title="Add Player"
                label="player name:"
                submit={(p) => this.addPlayer(p)}
            />
        </React.Fragment>);
    }
}
