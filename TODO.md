# TODO

* Code maintenance:
  * [ ] Update docstrings in `engine.py`
  * Unit tests and docstrings for:
    * [ ] `backend.py`
    * [ ] `cache.py`
    * [ ] `parser.py`
  * [ ] Update / extend django tests

* [x] Error reporting in GUI (popup)
* Player list:
  * [x] Make enumerated
  * [x] Add feature to correct spelling mistakes in player names
  * [x] Make player level adjustable after player was added
  * [x] Make add new player as a modal with name as input only
  * [x] Add feature to add / remove categories
* Matchmaking tab:
  * [x] Make results editable
  * [x] Add button to repeat draw
  * [x] Render ranking table in django template
  * [x] Enable multi-category visualization
  
* Remake of Frontend with ReactJS framework
  * Make React components:
    * [ ] draw / history item
    * [x] players table
    * [x] categories list
  * [ ] unit tests with fixed data
  * [ ] tests using python services

* Engine:
  * [x] consider drawing bye from pool of player with lowest seed
  * [x] Feature: Combination of games, e.g. foosball + beerpong. Make sure everyone plays similar amount of games in each category
