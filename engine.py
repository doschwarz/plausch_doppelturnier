import json
import math
import random
import operator
import itertools
import statistics

class Spieler:
    def __init__(self, name_, seed_=0, disciplines=None, late_entry_offset_=0):
        """
        :param name_: (str)
        :param seed_: (int) >= 0
        :param disciplines: (list) of unique (str)
        """
        if not isinstance(name_, str):
            raise TypeError("name_ must have type str")
        if (not isinstance(seed_, int)) or seed_ < 0:
            raise ValueError("seed_ must be an int >= 0")
        self.name = str(name_)
        self.late_entry_offset = int(late_entry_offset_)
        self.seed = int(seed_ + self.late_entry_offset)
        self.initial_seed = int(seed_)
        self.partner = list()
        self.einzelGegner = list()
        self.gegner1 = list()
        self.gegner2 = list()
        self.gewonnen_gegen = list()
        self.gewonnen_mit = list()
        self.discipline_record = dict()
        self.punktedifferenz = 0
        self.freilose = 0
        self.einzel = 0
        if disciplines is not None:
            assert isinstance(disciplines, (list, set)), "disciplines provided must be a list of (str)"
            for discipline in disciplines:
                self.add_discipline(discipline)
    
    def __repr__(self):
        return self.name + ': ' + str(self.seed)

    def __eq__(self, other):
        if isinstance(other, Spieler):
            return self.name == other.name
        return False

    def __hash__(self):
        return hash(json.dumps(self.to_dict()))


    def add_discipline(self, discipline_name):
        """
        Adds discipline
        
        :param discipline_name: (str)
        :returns: None
        """
        if not isinstance(discipline_name, str):
            raise TypeError("discipline_name must be type str")
        assert discipline_name not in self.discipline_record, "discipline '{}' already exists".format(discipline_name)
        self.discipline_record[discipline_name] = 0
    
    def remove_discipline(self, discipline_name):
        """
        Removes discpline from records

        :param discipline_name: (str)
        :returns: None
        """
        if not isinstance(discipline_name, str):
            raise TypeError("discipline_name must be type str")
        if discipline_name not in self.discipline_record:
            raise ValueError("Discipline '{}' does not exist in records".format(discipline_name))
        del self.discipline_record[discipline_name]

    def edit_seed(self, new_seed):
        """
        Changes player seed

        :param new_seed: (int)
        :returns: None
        """
        if not (isinstance(new_seed, int) and new_seed >= 0):
            raise ValueError("new_seed must be an int >= 0")
        self.seed += new_seed - self.initial_seed
        self.initial_seed = new_seed


    def edit_name(self, new_name):
        """
        Changes player name

        :param new_name: (str)
        :returns: None
        """
        if not isinstance(new_name, str):
            raise TypeError("new_name must be type str")
        self.name = new_name

    
    def change_name_in_records(self, old_name, new_name):
        """
        Changes a player name in all lists

        :param old_name: (str) name of player to be renamed
        :param new_name: (str) new player name
        :returns: None
        """
        for records in [self.partner, self.einzelGegner, self.gegner1,
                        self.gegner2, self.gewonnen_gegen, self.gewonnen_mit]:
            tmp = records.copy()
            for name in tmp:
                if name == old_name:
                    records.remove(old_name)
                    records.append(new_name)


    def enter_result(self, partner_, gegner1_, gegner2_, sieg_, diff_, discipline=None):
        """
        Save a doubles match result.

        :param partner_: (str) name of the doubles partner
        :param gegner1_: (str) name of the same strength opponent
        :param gegner2_: (str) name of the opponent's partner (same strength as partner_)
        :param sieg_: (bool) whether player has won the match
        :param diff_: (int) >= 0, point difference of the match
        """
        if not isinstance(sieg_, bool):
            raise TypeError("sieg_ must have type bool")
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")

        if discipline is not None:
            assert discipline in self.discipline_record, "discipline '{}' does not exist in records, must be added first".format(discipline)
            self.discipline_record[discipline] += 1
        elif len(self.discipline_record) > 1:
            raise AssertionError("Discipline must be provided")

        self.seed += int(sieg_)
        self.partner.append(partner_)
        self.gegner1.append(gegner1_)
        self.gegner2.append(gegner2_)
        self.punktedifferenz += (2 * int(sieg_) - 1) * diff_
        if sieg_:
            self.gewonnen_gegen.append(gegner1_)
            self.gewonnen_gegen.append(gegner2_)
            self.gewonnen_mit.append(partner_)


    def remove_result(self, partner_, gegner1_, gegner2_, sieg_, diff_, discipline=None):
        """
        Discard a doubles result entered with enter_result().
        Requires the exact same arguments to be used as when enter_result() was called.
        """
        if not isinstance(sieg_, bool):
            raise TypeError("sieg_ must have type bool")
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")

        if partner_ not in self.partner:
            raise ValueError("Partner '{}' not found".format(partner_))
        if gegner1_ not in self.gegner1:
            raise ValueError("Gegner 1 '{}' not found".format(gegner1_))
        if partner_ not in self.partner:
            raise ValueError("Gegner 2 '{}' not found".format(gegner2_))
        
        if discipline is not None:
            assert discipline in self.discipline_record, "discipline '{}' does not exist in records".format(discipline)
            assert self.discipline_record[discipline] >= 1, "No games recorded in discipline '{}'".format(discipline)
            self.discipline_record[discipline] -= 1
        elif len(self.discipline_record) > 1:
            raise AssertionError("Discipline must be provided")

        self.seed -= int(sieg_)
        self.partner.remove(partner_)
        self.gegner1.remove(gegner1_)
        self.gegner2.remove(gegner2_)
        self.punktedifferenz -= (2 * int(sieg_) - 1) * diff_
        if sieg_:
            self.gewonnen_gegen.remove(gegner1_)
            self.gewonnen_gegen.remove(gegner2_)
            self.gewonnen_mit.remove(partner_)


    def enter_result_Singles(self, gegner_, sieg_, diff_, discipline=None):
        """
        Save a singles match result.

        :param gegner: (str) name of the opponent
        :param sieg_: (bool) whether player has won the match
        :param diff_: (int) >= 0, point difference of the match
        """
        if not isinstance(sieg_, bool):
            raise TypeError("sieg_ must have type bool")
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")

        if discipline is not None:
            assert discipline in self.discipline_record, "discipline '{}' does not exist in records, must be added first".format(discipline)
            self.discipline_record[discipline] += 1
        elif len(self.discipline_record) > 1:
            raise AssertionError("Discipline must be provided")
        
        self.seed += int(sieg_)
        self.punktedifferenz += (2 * int(sieg_) - 1) * diff_
        self.einzel += 1
        self.einzelGegner.append(gegner_)
        if sieg_:
            self.gewonnen_gegen.append(gegner_)


    def remove_result_Singles(self, gegner_, sieg_, diff_, discipline=None):
        """
        Discard a singles result entered with enter_result_Singles().
        Requires the exact same arguments to be used as when enter_result_Singles() was called.
        """
        if not isinstance(sieg_, bool):
            raise TypeError("sieg_ must have type bool")
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")
        if gegner_ not in self.einzelGegner:
            raise ValueError("Gegner '{}' not found".format(gegner_))
        
        if discipline is not None:
            assert discipline in self.discipline_record, "discipline '{}' does not exist in records".format(discipline)
            assert self.discipline_record[discipline] >= 1, "No games recorded in discipline '{}'".format(discipline)
            self.discipline_record[discipline] -= 1
        elif len(self.discipline_record) > 1:
            raise AssertionError("Discipline must be provided")

        self.seed -= int(sieg_)
        self.punktedifferenz -= (2 * int(sieg_) - 1) * diff_
        self.einzel -= 1
        self.einzelGegner.remove(gegner_)
        if sieg_:
            self.gewonnen_gegen.remove(gegner_)


    def enter_result_Freilos(self):
        """
        Saves that player had a Freilos.
        """
        self.freilose += 1
        self.seed += 1


    def remove_result_Freilos(self):
        """
        Discards changes made by enter_result_Freilos()
        """
        self.freilose -= 1
        self.seed -= 1
    
    def to_dict(self):
        return {
            "name": self.name, "seed": self.seed, "initial_seed": self.initial_seed, "late_entry_offset": self.late_entry_offset,
            "partner": self.partner, "einzelGegner": self.einzelGegner, "gegner1": self.gegner1,
            "gegner2": self.gegner2, "gewonnen_gegen": self.gewonnen_gegen, "gewonnen_mit": self.gewonnen_mit,
            "punktedifferenz": self.punktedifferenz, "freilose": self.freilose, "einzel": self.einzel,
            "discipline_record": self.discipline_record
        }
    
    def from_dict(self, dict_):
        self.name = dict_['name']
        self.seed = dict_['seed']
        self.late_entry_offset = dict_["late_entry_offset"]
        self.initial_seed = dict_['initial_seed']
        self.partner = dict_['partner']
        self.einzelGegner = dict_['einzelGegner']
        self.gegner1 = dict_['gegner1']
        self.gegner2 = dict_['gegner2']
        self.gewonnen_gegen = dict_['gewonnen_gegen']
        self.gewonnen_mit = dict_['gewonnen_mit']
        self.punktedifferenz = dict_['punktedifferenz']
        self.freilose = dict_['freilose']
        self.einzel = dict_['einzel']
        self.discipline_record = dict_['discipline_record']


class Turnier:
    def __init__(self, spieler_=None, disciplines=None):
        """
        :param spieler: (list) [(name1, seed1), (name2, seed2), ...]
                            with name*: (str) player name
                                 seed*: (int >= 0) player strength category (higher -> stronger)
                    
        """
        self.spieler = list()        
        self.disciplines = list()
        self.removed = list()
        self.freilose = 0
        self.einzel = 0

        if disciplines is not None:
            assert isinstance(disciplines, list), "disciplines must be a (list)"
            for discipline in disciplines:
                self.add_discipline(discipline)
        
        if spieler_ is not None:
            for spieler in spieler_:
                if isinstance(spieler, tuple):
                    self.add_player(spieler[0], spieler[1])
                else:
                    self.add_player(spieler)


    def add_player(self, name_, seed_=0):
        """
        Adds a player to the tournament.

        :param name_: (str) name of the player
        :param seed_: (int) >= 0
        """
        for player in self.spieler + self.removed:
            if name_ == player.name:
                raise ValueError("Player with name '{}' already exists in the tournament".format(name_))
        
        tournament_started = False
        current_scores = dict()
        for player in self.spieler:
            if player.initial_seed != player.seed:
                tournament_started = True
            
            if player.initial_seed in current_scores:
                current_scores[player.initial_seed].append(player.seed)
            else:
                current_scores[player.initial_seed] = [player.seed]
        
        if tournament_started:
            offset = statistics.median([self.__already_started_offset(seed_ - 1, current_scores),
                                        self.__already_started_offset(seed_, current_scores),
                                        self.__already_started_offset(seed_ + 1, current_scores)])
        else:
            offset = 0
        
        self.spieler.append(Spieler(name_, seed_, self.disciplines, offset))


    def __already_started_offset(self, seed_, current_scores):
        if seed_ in current_scores:
            offset = round(statistics.mean(current_scores[seed_])) - seed_
        else:
            min_seed = min(current_scores)
            max_seed = max(current_scores)
            if seed_ < min_seed:
                offset = round(statistics.mean(current_scores[min_seed])) - min_seed
            elif seed_ > max_seed:
                offset = round(statistics.mean(current_scores[max_seed])) - max_seed            
            else:
                just_smaller_seed = max(s for s in current_scores if s < seed_)
                just_larger_seed = min(s for s in current_scores if s > seed_)
                avg_just_smaller = statistics.mean(current_scores[just_smaller_seed])
                avg_just_larger = statistics.mean(current_scores[just_larger_seed])
                offset = round((avg_just_smaller * (just_larger_seed - seed_) + avg_just_larger / (seed_ - just_smaller_seed))\
                                / (just_larger_seed - just_smaller_seed)) - seed_
        return offset
    
    
    def edit_player(self, name_, new_name=None, new_seed=None):
        player = self.__playerSearch(name_, self.spieler)
        if new_name is not None and name_ != new_name:
            raised = False
            try:
                self.__playerSearch(new_name)
            except ValueError as e:
                if str(e) == 'Name "{}" not found'.format(new_name):
                    raised = True
            if not raised:
                raise ValueError('New name "{}" already exists in the tournament'.format(new_name))
                
            player.edit_name(new_name)
            for spieler in self.spieler:
                spieler.change_name_in_records(name_, new_name)

        if new_seed is not None and player.seed != new_seed:
            player.edit_seed(new_seed)


    def remove_player(self, name_):
        """
        Removes a player from the draw. He/she will still be displayed in the standings.

        :param name_: (str) name of the player that shall be removed
        """
        removed = False
        for spieler in self.spieler.copy():
            if spieler.name == name_:
                if removed:
                    raise RuntimeError('Multiple players have the same name "{}"'.format(name_))
                self.spieler.remove(spieler)
                self.removed.append(spieler)
                removed = True
        if not removed:
            raise ValueError('Name "{}" not found'.format(name_))
    

    def add_discipline(self, discipline_name):
        """
        Adds a discipline
        
        :param discipline_name: (str)
        :returns: None
        """
        if not isinstance(discipline_name, str):
            raise TypeError("discipline_name must be type str")
        assert discipline_name not in self.disciplines, "discipline '{}' already exists".format(discipline_name)
        self.disciplines.append(discipline_name)
        for player in self.spieler:
            player.add_discipline(discipline_name)
    

    def remove_discipline(self, discipline_name):
        """
        Removes a discipline from the tournament

        :param discipline_name: (str)
        :returns: None
        """
        if not isinstance(discipline_name, str):
            raise TypeError("discipline_name must be type str")
        if discipline_name not in self.disciplines:
            raise ValueError("Discipline '{}' does not exist in records".format(discipline_name))
        self.disciplines.remove(discipline_name)
        for player in self.spieler:
            player.remove_discipline(discipline_name)


    def enter_result(self, team1P1_, team1P2_, team2P1_, team2P2_, victor_, diff_, discipline=None):
        """
        Save a doubles match result. Use victor_ = 1 if team 1 has won, victor_ = 2 otherwise.

        :param team1P1_: (str) name of player 1 in team 1
        :param team1P2_: (str) name of player 2 in team 1
        :param team2P1_: (str) name of player 1 in team 2
        :param team2P2_: (str) name of player 2 in team 2
        :param victor_: (int) in [1, 2]
        :param diff_: (int) >= 0, point difference of the match
        :param discipline: (str) discipline of the game played
        """
        if victor_ not in [1, 2]:
            raise ValueError('victor has to be either 1 or 2.')
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")
        if discipline is not None:
            assert discipline in self.disciplines, "discipline '{}' does not exist in records, must be added first".format(discipline)
        elif len(self.disciplines) > 1:
            raise AssertionError("Discipline must be provided")
        
        team1P1 = self.__playerSearch(team1P1_)
        team1P2 = self.__playerSearch(team1P2_)
        team2P1 = self.__playerSearch(team2P1_)
        team2P2 = self.__playerSearch(team2P2_)

        team1P1.enter_result(team1P2.name, team2P1.name, team2P2.name, victor_ == 1, diff_, discipline)
        team1P2.enter_result(team1P1.name, team2P2.name, team2P1.name, victor_ == 1, diff_, discipline)
        team2P1.enter_result(team2P2.name, team1P1.name, team1P2.name, victor_ == 2, diff_, discipline)
        team2P2.enter_result(team2P1.name, team1P2.name, team1P1.name, victor_ == 2, diff_, discipline)


    def remove_result(self, team1P1_, team1P2_, team2P1_, team2P2_, victor_, diff_, discipline=None):
        """
        Discard a doubles result entered with enter_result().
        Requires the exact same arguments to be used as when enter_result() was called.
        """
        if victor_ not in [1, 2]:
            raise ValueError('victor_ has to be either 1 or 2.')
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")
        if discipline is not None:
            assert discipline in self.disciplines, "discipline '{}' does not exist in records".format(discipline)
        elif len(self.disciplines) > 1:
            raise AssertionError("Discipline must be provided")

        team1P1 = self.__playerSearch(team1P1_)
        team1P2 = self.__playerSearch(team1P2_)
        team2P1 = self.__playerSearch(team2P1_)
        team2P2 = self.__playerSearch(team2P2_)

        team1P1.remove_result(team1P2.name, team2P1.name, team2P2.name, victor_ == 1, diff_, discipline)
        team1P2.remove_result(team1P1.name, team2P2.name, team2P1.name, victor_ == 1, diff_, discipline)
        team2P1.remove_result(team2P2.name, team1P1.name, team1P2.name, victor_ == 2, diff_, discipline)
        team2P2.remove_result(team2P1.name, team1P2.name, team1P1.name, victor_ == 2, diff_, discipline)


    def enter_result_Singles(self, player1_, player2_, victor_, diff_, discipline=None):
        """
        Saves a singles match result. Use victor = 1 if player1_ has won, victor = 2 otherwise.

        :param player1_: (str) name of player 1
        :param player2_: (str) name of player 2
        :param victor_: (int) in [1, 2]
        :param diff_: (int) >= 0, point difference of the match
        :param discipline: (str) discipline of the game played
        """
        if victor_ not in [1, 2]:
            raise ValueError('victor_ has to be either 1 or 2.')
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")
        if discipline is not None:
            assert discipline in self.disciplines, "discipline '{}' does not exist in records, must be added first".format(discipline)
        elif len(self.disciplines) > 1:
            raise AssertionError("Discipline must be provided")

        player1 = self.__playerSearch(player1_)
        player2 = self.__playerSearch(player2_)

        self.einzel += 1
        player1.enter_result_Singles(player2.name, victor_ == 1, diff_, discipline)
        player2.enter_result_Singles(player1.name, victor_ == 2, diff_, discipline)


    def remove_result_Singles(self, player1_, player2_, victor_, diff_, discipline=None):
        """
        Discard a singles result entered with enter_result_Singles().
        Requires the exact same arguments to be used as when enter_result_Singles() was called.
        """
        if victor_ not in [1, 2]:
            raise ValueError('victor has to be either 1 or 2.')
        if (not isinstance (diff_, int)) or diff_ < 0:
            raise ValueError("diff_ has to be an int >= 0")
        if discipline is not None:
            assert discipline in self.disciplines, "discipline '{}' does not exist in records".format(discipline)
        elif len(self.disciplines) > 1:
            raise AssertionError("Discipline must be provided")

        player1 = self.__playerSearch(player1_)
        player2 = self.__playerSearch(player2_)

        self.einzel -= 1
        player1.remove_result_Singles(player2.name, victor_ == 1, diff_, discipline)
        player2.remove_result_Singles(player1.name, victor_ == 2, diff_, discipline)


    def enter_result_Freilos(self, player_):
        """
        Saves the event that player_ had a Freilos in this round.

        :param player_: (str) name of the player
        """
        player = self.__playerSearch(player_)
        self.freilose += 1
        player.enter_result_Freilos()


    def remove_result_Freilos(self, player_):
        """
        Discards the changes made by enter_result_Freilos(), if the exact same name is used.
        """
        player = self.__playerSearch(player_)
        self.freilose -= 1
        player.remove_result_Freilos()


    def __playerSearch(self, name_, list_=None):
        found = False
        if list_ is None:
            list_ = self.spieler + self.removed
        for spieler in list_:
            if spieler.name == name_:
                if found:
                    raise RuntimeError('Multiple players have the same name "{}"'.format(name_))
                found = True
                result = spieler
        if not found:
            raise ValueError('Name "{}" not found'.format(name_))
        return result


    def get_player_list(self):
        players = list()
        for player in self.spieler:
            players.append({"name": player.name, "seed": player.initial_seed})
        removed = list()
        for player in self.removed:
            removed.append({"name": player.name, "seed": player.initial_seed})
        return {"players": players, "removed": removed}


    def calculate_ranking(self, method_=0):
        """
        Calculates the current ranking
        If method = 0, points are only counted for wins. Else, both wins and losses count.

        :param method_: (int)
        """
        spieler = self.spieler + self.removed
        siege = [int(x.seed) - int(x.initial_seed) for x in spieler]
        results = dict()
        for player in spieler:
            siege = int(player.seed) - int(player.initial_seed)
            diff = int(player.punktedifferenz)
            if method_ == 0:
                plus = [self.__playerSearch(name) for name in player.gewonnen_gegen]
                minus = [self.__playerSearch(name) for name in player.gewonnen_mit]
            else:
                plus = [self.__playerSearch(name) for name in (player.gegner1 + player.gegner2)]
                minus = [self.__playerSearch(name) for name in player.partner]

            punkte = sum([x.seed - x.initial_seed - x.late_entry_offset for x in plus]) - sum([x.seed - x.initial_seed - x.late_entry_offset for x in minus])
            results[player.name] = {
                "Name": player.name,
                "Siege": siege,
                "Punkte": punkte,
                "Punktedifferenz": diff,
                "removed": player in self.removed,
                "rankingValue": int(siege * 1e9 + punkte * 1e6 + diff * 1e3 + (0 if self.removed else 1))
            }
        
        order = {name: vals["rankingValue"] for name, vals in results.items()}
        rank = 1
        ranking_list = list()
        ordered_name_list = [name for name, _ in sorted(order.items(), key=operator.itemgetter(1), reverse=True)]
        for i, name in enumerate(ordered_name_list):
            if i == 0 or (results[ordered_name_list[i-1]]["rankingValue"] - results[name]["rankingValue"] > 2):
                rank = i + 1
            results[name]['Rang'] = rank
            ranking_list.append(results[name])

        return ranking_list
    

    def __theoretical_min_disciplines_number(self):
        num_rounds = len(self.spieler[0].partner) + self.spieler[0].einzel + self.spieler[0].freilose + 1
        num_disciplines = max(len(self.disciplines), 1)
        ratio = num_rounds // num_disciplines
        remainder = num_rounds % num_disciplines

        min_points_per_player = (num_disciplines - remainder) * ratio**2 + remainder * (ratio + 1)**2
        min_points = len(self.spieler) * min_points_per_player - self.freilose * (ratio + 1)**2

        return min_points

    
    def make_multi_discipline_draw(self, maxIterations=10, maxDrawsPerIteration=20):
        min_disciplines_number = self.__theoretical_min_disciplines_number()
        games_per_round = len(self.spieler) // 4 + (len(self.spieler) % 4) // 2
        max_per_discipline = games_per_round // len(self.disciplines)
        if games_per_round % len(self.disciplines) > 0:
            max_per_discipline += 1

        best_draw = None
        best_discipline_number_sum = None

        for lala in range(maxIterations):
            draw = self.make_draw(maxDrawsPerIteration)
            if 'singles' in draw:
                all_games = [(0, [self.__playerSearch(player, self.spieler) for player in draw['singles']])]
            else:
                all_games = list()
            all_games += [(i + 1, [self.__playerSearch(player, self.spieler) for pair in pairs for player in pair]) for i, pairs in enumerate(draw['doubles'])]

            for permutation in itertools.permutations(all_games): # TODO: Test performace of this
                random.shuffle(self.disciplines)
                multi_draw = {discipline: list() for discipline in self.disciplines}
                discipline_number_sum = 0

                for i, game in permutation:
                    best_discipline = None
                    best_discipline_number = None
                    for discipline in self.disciplines:
                        if len(multi_draw[discipline]) >= max_per_discipline:
                            continue
                        other_disciplines = list(self.disciplines)
                        other_disciplines.remove(discipline)
                        new_discipline_number = sum((player.discipline_record[discipline] + 1)**2 + sum(player.discipline_record[d]**2 for d in other_disciplines) for player in game)
                        if best_discipline is None or new_discipline_number < best_discipline_number:
                            best_discipline_number = new_discipline_number
                            best_discipline = discipline
                        elif new_discipline_number == best_discipline_number and len(multi_draw[best_discipline]) > len(multi_draw[discipline]):
                            best_discipline = discipline
                    
                    multi_draw[best_discipline].append(i)
                    discipline_number_sum += best_discipline_number
                
                if best_draw is None or discipline_number_sum < best_discipline_number_sum:
                    best_draw = {"doubles": {discipline: list() for discipline in self.disciplines}}
                    if 'bye' in draw:
                        best_draw['bye'] = draw['bye']

                    for discipline in multi_draw:
                        for i in multi_draw[discipline]:
                            if i == 0:
                                assert "singles" not in best_draw, "i = 0 should only be the case for the one singles match"
                                assert "singles" in draw, "i = 0 should only occur if there is a singles match"
                                best_draw['singles'] = {discipline: draw['singles']}
                            else:
                                best_draw['doubles'][discipline].append(draw['doubles'][i - 1])

                    best_discipline_number_sum = discipline_number_sum

                if best_discipline_number_sum <= min_disciplines_number:
                    break

            if best_discipline_number_sum <= min_disciplines_number:
                break
        
        return best_draw


    def make_draw(self, maxIterations=100):
        """
        Makes a complete draw.

        :param maxIterations: (int) > 0
        returns: (dict) {doubles: (list), singles: (list), bye: (str)}
        """

        counter = 0
        bye = None
        singles = None
        while counter < maxIterations:
            counter += 1
            players = self.spieler.copy()

            if len(self.spieler) % 2 == 1:
                bye = self.__draw_bye()
                players.remove(bye)
            
            pairs = self.__make_pairs(players, counter / maxIterations)
            if pairs is None:
                continue
            
            if len(pairs) % 2 == 1:
                singles = self.__draw_singles(pairs)
                if singles is None:
                    continue
                pairs.remove(singles)
            
            doubles = self.__draw_doubles(pairs)
            if doubles is not None:
                result = {"doubles": doubles}
                if singles is not None:
                    result["singles"] = [singles[0].name, singles[1].name]
                if bye is not None:
                    result["bye"] = bye.name
            
                return result
        
        raise RuntimeError("No new draw found, all possible combinations have been played")


    def __draw_bye(self):
        assert len(self.spieler) % 2 == 1
        maxByes = math.floor(self.freilose/len(self.spieler))
        maxSeed = max(x.seed for x in self.spieler)
        
        groups = [[x for x in self.spieler if x.seed == numberOfVictories] for numberOfVictories in range(maxSeed + 1)]
        for group in groups:
            random.shuffle(group)
            for player in group:
                if player.freilose <= maxByes:
                    return player
        
        raise RuntimeError("Probably a bug; according to total number of byes in tournament so far, there should be a player with {:d} byes, but didn't find it".format(maxByes))


    def __make_pairs(self, players, counter_fraction=0):
        """

        """
        assert len(players) % 2 == 0
        maxSeed = max(x.seed for x in players)
        groups = [[x for x in players if x.seed == numberOfVictories] for numberOfVictories in reversed(range(maxSeed + 1))]

        hold = set()
        pairs = set()
        for group in groups:
            random.shuffle(group)
            if len(hold) > 0:
                group = list(hold) + group
                hold = set()

            while len(group) > 1:
                player1 = group[0]
                opponents = group[1:]
                player2 = random.choice(opponents)
                foundMatch = False
                while len(opponents) > 0:
                    already_faced = player1.gegner1
                    if counter_fraction < 0.75:
                        already_faced += player1.gegner2
                        if counter_fraction < 0.5:
                            already_faced += player1.einzelGegner
                    if player2.name in already_faced:
                        opponents.remove(player2)
                        if len(opponents) > 0:
                            player2 = random.choice(opponents)
                        else:
                            break
                    else:
                        pairs.add((player1, player2))
                        foundMatch = True
                        break

                if foundMatch:
                    group.remove(player1)
                    group.remove(player2)
                else:
                    hold.add(player1)
                    group.remove(player1)
            if len(group) == 1:
                hold.update(group)

        if len(hold) > 0:
            if counter_fraction >= 0.75 and len(hold) == 2:
                pairs.add(tuple(hold))
            else:
                return None            

        return pairs


    def __draw_singles(self, pairs):
        """
        :param pairs: set(touple(Player, Player))
        """
        maxSingles = math.floor(2*self.einzel/len(self.spieler))
        for pair in random.sample(pairs, len(pairs)):
            if pair[0].einzel <= maxSingles and pair[1].einzel <= maxSingles and (pair[0].name not in pair[1].einzelGegner):
                assert pair[1].name not in pair[0].einzelGegner
                return pair
        return None


    def __draw_doubles(self, pairs):
        """
        :param pairs: set(touple(Player, Player))
        :returns: list([[Player, Player], [Player, Player]])
        """
        assert len(pairs) % 2 == 0
        doubles = list()
        no_partners_found = list()

        while len(pairs) > 1:
            pair1 = random.sample(pairs, 1)[0]
            pairs.remove(pair1)

            others = pairs.copy()
            found = False
            for pair2 in random.sample(others, len(others)):
                if pair2[1].name in pair1[0].partner or pair2[0].name in pair1[1].partner\
                    or pair2[0].name in pair1[0].gegner2 or pair2[1].name in pair1[1].gegner2:
                    if pair2[0].name in pair1[0].partner or pair2[1].name in pair1[1].partner\
                        or pair2[1].name in pair1[0].gegner2 or pair2[0].name in pair1[1].gegner2:
                        continue
                    else:
                        found = True
                        pairs.remove(pair2)
                        doubles.append([[pair1[0].name, pair2[0].name], [pair1[1].name, pair2[1].name]])
                        break
                else:
                    found = True
                    pairs.remove(pair2)
                    doubles.append([[pair1[0].name, pair2[1].name], [pair1[1].name, pair2[0].name]])
                    break
            
            if not found:
                no_partners_found.append(pair1)

        if len(pairs) > 0:
            assert len(pairs) == 1
            no_partners_found.append(pairs.pop())

        if len(no_partners_found) > 0:
            # TODO: Try to match leftover pairs
            return None
        
        return doubles

    
    def to_dict(self):
        return {
            "spieler": [s.to_dict() for s in self.spieler],
            "removed": [s.to_dict() for s in self.removed],
            "freilose": self.freilose,
            "einzel": self.einzel,
            "disciplines": self.disciplines
        }
    
    def from_dict(self, dict_):
        self.freilose = dict_['freilose']
        self.einzel = dict_['einzel']
        self.disciplines = dict_["disciplines"]

        self.removed = list()
        self.spieler = list()
        for player in dict_['removed']:
            dummy = Spieler("")
            dummy.from_dict(player)
            self.removed.append(dummy)
        for player in dict_['spieler']:
            dummy = Spieler("")
            dummy.from_dict(player)
            self.spieler.append(dummy)
