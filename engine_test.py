from unittest import TestCase
from engine import Turnier, Spieler
import json

class SpielerTests(TestCase):
    def test_initialization(self):
        name = "dummy Name"
        seed = 2
        disciplines = ["a", "b"]

        spieler = Spieler(name)
        self.assertEqual(spieler.name, name)
        self.assertEqual(spieler.seed, 0)
        self.assertEqual(spieler.initial_seed, 0)
        self.assertListEqual(spieler.partner, list())
        self.assertListEqual(spieler.einzelGegner, list())
        self.assertListEqual(spieler.gegner1, list())
        self.assertListEqual(spieler.gegner2, list())
        self.assertListEqual(spieler.gewonnen_gegen, list())
        self.assertListEqual(spieler.gewonnen_mit, list())
        self.assertDictEqual(spieler.discipline_record, dict())
        self.assertEqual(spieler.punktedifferenz, 0)
        self.assertEqual(spieler.freilose, 0)
        self.assertEqual(spieler.einzel, 0)

        spieler = Spieler(name, seed)
        self.assertEqual(spieler.name, name)
        self.assertEqual(spieler.seed, seed)
        self.assertEqual(spieler.initial_seed, seed)

        spieler = Spieler(name, seed, disciplines)
        self.assertDictEqual(spieler.discipline_record, {"a": 0, "b": 0})

    
    def test_repr(self):
        spieler = Spieler("dummy", 3)
        self.assertEqual(str(spieler), "dummy: 3")
    
    def test_edit_player(self):
        spieler = Spieler("dummy", 3)

        new_name = "lala"
        spieler.edit_name(new_name)
        self.assertEqual(spieler.name, new_name)

        new_seed = 1
        self.assertEqual(spieler.seed, 3)
        spieler.edit_seed(new_seed)
        self.assertEqual(spieler.seed, new_seed)
        self.assertEqual(spieler.initial_seed, new_seed)
    
    def test_add_remove_discipline(self):
        spieler = Spieler("dummy")
        name = "nana"
        spieler.add_discipline(name)
        self.assertDictEqual(spieler.discipline_record, {name: 0})
        self.assertRaises(ValueError, spieler.remove_discipline, name + 'i')
        spieler.remove_discipline(name)
        self.assertDictEqual(spieler.discipline_record, dict())

    def test_change_name_in_records(self):
        spieler = Spieler("dummy")
        spieler.enter_result("partner1", "gegner11", "gegner21", True, 2)
        spieler.enter_result("partner2", "gegner12", "gegner22", False, 3)
        spieler.enter_result_Singles("gegner11", True, 5)
        spieler.enter_result_Singles("gegner22", False, 4)

        new_opponent = "new_opponent"
        new_partner = "new_partner"
        spieler.change_name_in_records("gegner11", new_opponent)
        spieler.change_name_in_records("partner1", new_partner)

        self.assertNotIn("gegner11", spieler.partner + spieler.einzelGegner + spieler.gegner1 + spieler.gegner2 + spieler.gewonnen_gegen + spieler.gewonnen_mit)
        self.assertNotIn("partner1", spieler.partner + spieler.einzelGegner + spieler.gegner1 + spieler.gegner2 + spieler.gewonnen_gegen + spieler.gewonnen_mit)

        self.assertIn(new_opponent, spieler.gegner1)
        self.assertIn(new_opponent, spieler.gewonnen_gegen)
        self.assertIn(new_opponent, spieler.einzelGegner)
        self.assertNotIn(new_opponent, spieler.partner + spieler.gegner2, spieler.gewonnen_mit)

        self.assertIn(new_partner, spieler.partner)
        self.assertIn(new_partner, spieler.gewonnen_mit)
        self.assertNotIn(new_partner, spieler.einzelGegner + spieler.gegner1 + spieler.gegner2 + spieler.gewonnen_gegen)

    def test_enter_remove_result(self):
        spieler = Spieler("dummy")

        self.assertRaises(TypeError, spieler.enter_result, "", "", "", 3, 1)
        self.assertRaises(ValueError, spieler.enter_result, "", "", "", True, -1)

        spieler.enter_result("partner1", "gegner11", "gegner21", True, 2)
        spieler.enter_result("partner2", "gegner12", "gegner22", False, 3)
        spieler.enter_result("partner3", "gegner13", "gegner23", True, 4)
        self.assertListEqual(sorted(spieler.partner), sorted(["partner1", "partner2", "partner3"]))
        self.assertListEqual(sorted(spieler.gegner1), sorted(["gegner11", "gegner12", "gegner13"]))
        self.assertListEqual(sorted(spieler.gegner2), sorted(["gegner21", "gegner22", "gegner23"]))
        self.assertEqual(spieler.punktedifferenz, 3)
        self.assertEqual(spieler.seed, 2)
        self.assertListEqual(sorted(spieler.gewonnen_gegen), sorted(["gegner11", "gegner21", "gegner13", "gegner23"]))
        self.assertListEqual(sorted(spieler.gewonnen_mit), sorted(["partner1", "partner3"]))

        self.assertRaises(TypeError, spieler.remove_result, "", "", "", 3, 1)
        self.assertRaises(ValueError, spieler.remove_result, "", "", "", True, -1)
        self.assertRaises(ValueError, spieler.remove_result, "", "", "", True, 2)

        spieler.remove_result("partner1", "gegner11", "gegner21", True, 2)
        self.assertListEqual(sorted(spieler.partner), sorted(["partner2", "partner3"]))
        self.assertListEqual(sorted(spieler.gegner1), sorted(["gegner12", "gegner13"]))
        self.assertListEqual(sorted(spieler.gegner2), sorted(["gegner22", "gegner23"]))
        self.assertEqual(spieler.punktedifferenz, 1)
        self.assertEqual(spieler.seed, 1)
        self.assertListEqual(sorted(spieler.gewonnen_gegen), sorted(["gegner13", "gegner23"]))
        self.assertListEqual(spieler.gewonnen_mit, ["partner3"])
    
    def test_enter_remove_singles(self):
        spieler = Spieler("dummy")

        self.assertRaises(TypeError, spieler.enter_result_Singles, "", 2, 1)
        self.assertRaises(ValueError, spieler.enter_result_Singles, "", True, -1)

        spieler.enter_result_Singles("gegner1", True, 5)
        spieler.enter_result_Singles("gegner2", False, 4)
        spieler.enter_result_Singles("gegner3", False, 3)
        spieler.enter_result_Singles("gegner4", True, 2)
        spieler.enter_result("partner1", "gegner51", "gegner52", True, 1)
        spieler.enter_result("partner2", "gegner61", "gegner62", False, 1)

        self.assertListEqual(sorted(spieler.einzelGegner), sorted(["gegner1", "gegner2", "gegner3", "gegner4"]))
        self.assertListEqual(sorted(spieler.gewonnen_gegen), sorted(["gegner1", "gegner4", "gegner51", "gegner52"]))
        self.assertEqual(spieler.seed, 3)
        self.assertEqual(spieler.punktedifferenz, 0)
        self.assertEqual(spieler.einzel, 4)

        self.assertRaises(TypeError, spieler.remove_result_Singles, "gegner1", 4, 5)
        self.assertRaises(ValueError, spieler.remove_result_Singles, "gegner1", True, -5)
        self.assertRaises(ValueError, spieler.remove_result_Singles, "asdf", True, 1)

        spieler.remove_result_Singles("gegner3", False, 3)

        self.assertListEqual(sorted(spieler.einzelGegner), sorted(["gegner1", "gegner2", "gegner4"]))
        self.assertListEqual(sorted(spieler.gewonnen_gegen), sorted(["gegner1", "gegner4", "gegner51", "gegner52"]))
        self.assertEqual(spieler.punktedifferenz, 3)
        self.assertEqual(spieler.einzel, 3)
        self.assertEqual(spieler.seed, 3)

    def test_enter_remove_freilos(self):
        spieler = Spieler("dummy")

        spieler.enter_result_Freilos()
        self.assertEqual(spieler.freilose, 1)
        self.assertEqual(spieler.seed, 1)

        spieler.remove_result_Freilos()
        self.assertEqual(spieler.freilose, 0)
        self.assertEqual(spieler.seed, 0)

    def test_save_to_dict(self):
        spieler = Spieler("dummy", 1)
        spieler.enter_result_Freilos()
        spieler.enter_result_Singles("Einzelgegner", True, 4)
        spieler.enter_result("Doppelpartner", "Doppelgegner", "DoppelgegnerPartner", True, 8)
        dict_repr = spieler.to_dict()
        json.dumps(dict_repr)

        replica = Spieler("la")
        replica.from_dict(dict_repr)

        self.assertEqual(spieler.name, replica.name)
        self.assertEqual(spieler.seed, replica.seed)
        self.assertEqual(spieler.initial_seed, replica.initial_seed)
        self.assertEqual(spieler.late_entry_offset, replica.late_entry_offset)
        self.assertListEqual(spieler.partner, replica.partner)
        self.assertListEqual(spieler.einzelGegner, replica.einzelGegner)
        self.assertListEqual(spieler.gegner1, replica.gegner1)
        self.assertListEqual(spieler.gegner2, replica.gegner2)
        self.assertListEqual(spieler.gewonnen_gegen, replica.gewonnen_gegen)
        self.assertListEqual(spieler.gewonnen_mit, replica.gewonnen_mit)
        self.assertEqual(spieler.punktedifferenz, replica.punktedifferenz)
        self.assertEqual(spieler.freilose, replica.freilose)
        self.assertEqual(spieler.einzel, replica.einzel)

class TunierTests(TestCase):
    def test_initialization(self):
        turnier1 = Turnier()
        self.assertEqual(len(turnier1.spieler), 0)
        self.assertEqual(len(turnier1.removed), 0)
        self.assertGreaterEqual(turnier1.freilose, 0)
        self.assertGreaterEqual(turnier1.einzel, 0)

        spieler = [('a', 0), ('b', 1), ('c', 1), ('d', 0), ('e', 1), ('f', 2), ('g', 1), ('h', 0), ('i', 2), ('j', 0), ('k', 2)]
        turnier2 = Turnier(spieler)
        self.assertListEqual(turnier2.spieler, [Spieler(x[0], x[1]) for x in spieler])
        self.assertEqual(len(turnier2.removed), 0)
    
    def test_player_search(self):
        spieler = [('a', 0), ('b', 1), ('c', 1), ('c', 2)]
        turnier = Turnier(spieler[:-1])

        self.assertEqual(turnier._Turnier__playerSearch('b'), Spieler('b', 1))
        self.assertRaises(ValueError, turnier._Turnier__playerSearch, "asdf")

        playerList = [Spieler(x[0], x[1]) for x in spieler]
        self.assertEqual(turnier._Turnier__playerSearch('a', playerList), Spieler('a', 0))
        self.assertRaises(ValueError, turnier._Turnier__playerSearch, 'asdf', playerList)
        self.assertRaises(RuntimeError, turnier._Turnier__playerSearch, 'c', playerList)

    def test_add_remove_player(self):
        turnier = Turnier([('a', 0), ('b', 1)])
        turnier.add_player("c", 1)
        self.assertListEqual(turnier.spieler, [Spieler(x[0], x[1]) for x in [('a', 0), ('b', 1), ('c', 1)]])
        
        self.assertRaises(ValueError, turnier.remove_player, "asdf")
        turnier.remove_player("b")
        self.assertListEqual(turnier.spieler, [Spieler(x[0], x[1]) for x in [('a', 0), ('c', 1)]])
        self.assertListEqual(turnier.removed, [Spieler('b', 1)])

        turnier.enter_result_Singles('a', 'c', 1, 3)
        turnier.remove_player('a')
    
    def test_edit_player(self):
        spieler = [('a', 0), ('b', 1), ('c', 2), ('d', 0)]
        turnier = Turnier(spieler)

        self.assertRaises(ValueError, turnier.edit_player, "la")
        self.assertRaises(ValueError, turnier.edit_player, "a", "b")
        self.assertRaises(ValueError, turnier.edit_player, "a", "a", -2)
        self.assertRaises(ValueError, turnier.edit_player, "a", "a", 3.5)

        turnier.enter_result("a", "b", "c", "d", 1, 2)

        turnier.edit_player('a', 'la')
        self.assertRaises(ValueError, turnier._Turnier__playerSearch, 'a')
        turnier._Turnier__playerSearch('la')
        b = turnier._Turnier__playerSearch('b')
        c = turnier._Turnier__playerSearch('c')
        d = turnier._Turnier__playerSearch('d')

        self.assertIn('la', b.partner)
        self.assertNotIn('a', b.partner)
        self.assertIn('la', c.gegner1)
        self.assertNotIn('a', c.gegner1)
        self.assertIn('la', d.gegner2)
        self.assertNotIn('a', d.gegner2)
        self.assertIn('la', b.gewonnen_mit)
        self.assertNotIn('a', b.gewonnen_mit)

    def __validate_draw(self, draw, names):
        json.dumps(draw, allow_nan=False) # assert result is json serializable
        names_ = list()
        if 'bye' in draw:
            self.assertIn(draw['bye'], names)
            names_.append(draw['bye'])

        if 'singles' in draw:
            self.assertIsInstance(draw['singles'], list)
            self.assertEqual(len(draw['singles']), 2)
            self.assertIn(draw['singles'][0], names)
            names_.append(draw['singles'][0])
            self.assertIn(draw['singles'][1], names)
            names_.append(draw['singles'][1])

        self.assertIsInstance(draw['doubles'], list)
        for pair in draw['doubles']:
            self.assertIsInstance(pair, list)
            self.assertEqual(len(pair), 2)
            for team in pair:
                self.assertIsInstance(team, list)
                self.assertEqual(len(team), 2)
                for name in team:
                    self.assertIn(name, names)
                    names_.append(name)

        self.assertEqual(len(names_), len(set(names_)))
        self.assertSetEqual(set(names_), names)

    def test_draw(self):
        spieler = [('a', 0), ('b', 1), ('c', 1), ('d', 0), ('e', 1), ('f', 2), ('g', 1), ('h', 0), ('i', 2), ('j', 0), ('k', 2)]
        names = {n for n, k in spieler}
        turnier = Turnier(spieler)

        draw = turnier.make_draw()
        self.assertSetEqual(set(draw), {'bye', 'singles', 'doubles'})
        self.__validate_draw(draw, names)

        turnier.remove_player('h')
        names.remove('h')
        draw = turnier.make_draw()
        self.assertSetEqual(set(draw), {'singles', 'doubles'})
        self.__validate_draw(draw, names)

        turnier.remove_player('a')
        names.remove('a')
        draw = turnier.make_draw()
        self.assertSetEqual(set(draw), {'bye', 'doubles'})
        self.__validate_draw(draw, names)
        
        turnier.remove_player('f')
        names.remove('f')
        draw = turnier.make_draw()
        self.assertSetEqual(set(draw), {'doubles'})
        self.__validate_draw(draw, names)

    def __validate_multi_draw(self, draw, names, categories):
        json.dumps(draw, allow_nan=False) # assert result is json serializable
        names_ = list()
        category_counter = {cat: 0 for cat in categories}
        if 'bye' in draw:
            self.assertIn(draw['bye'], names)
            names_.append(draw['bye'])
        if 'singles' in draw:
            self.assertIsInstance(draw['singles'], dict)

            category = list(draw['singles'].keys())[0]
            self.assertIn(category, categories)
            category_counter[category] += 1

            singles_match = list(draw['singles'].values())[0]
            self.assertEqual(len(singles_match), 2)
            self.assertIn(singles_match[0], names)
            names_.append(singles_match[0])
            self.assertIn(singles_match[1], names)
            names_.append(singles_match[1])
        
        self.assertIsInstance(draw['doubles'], dict)
        for category, games in draw['doubles'].items():
            self.assertIn(category, categories)
            self.assertIsInstance(games, list)
            category_counter[category] += len(games)

            for pair in games:
                self.assertIsInstance(pair, list)
                self.assertEqual(len(pair), 2)
                for team in pair:
                    self.assertIsInstance(team, list)
                    self.assertEqual(len(team), 2)
                    for name in team:
                        self.assertIn(name, names)
                        names_.append(name)

        self.assertEqual(len(names_), len(set(names_)))
        self.assertSetEqual(set(names_), names)
        self.assertGreaterEqual(min(category_counter.values()), max(category_counter.values()) - 1)

    def test_multi_discipline_draw(self):
        spieler = [('a', 0), ('b', 1), ('c', 1), ('d', 0), ('e', 1), ('f', 2), ('g', 1), ('h', 0), ('i', 2), ('j', 0), ('k', 2)]
        names = {n for n, k in spieler}
        disciplines = ["BP", "FB"]
        turnier = Turnier(spieler, disciplines)

        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)
        turnier.enter_result("j", "d", "a", "h", 1, 2, "BP")
        turnier.enter_result("g", "k", "c", "i", 1, 3, "FB")
        turnier.enter_result_Singles("f", "e", 1, 3, "FB")
        turnier.enter_result_Freilos("b")

        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)
        turnier.enter_result("b", "c", "i", "e", 1, 2, "BP")
        turnier.enter_result("k", "d", "f", "g", 1, 3, "BP")
        turnier.enter_result_Singles("a", "h", 1, 3, "FB")
        turnier.enter_result_Freilos("j")

        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)
        turnier.enter_result("e", "f", "h", "i", 1, 2, "BP")
        turnier.enter_result("c", "j", "a", "d", 1, 3, "FB")
        turnier.enter_result_Singles("k", "b", 1, 3, "FB")
        turnier.enter_result_Freilos("g")

        for name in names:
            player = turnier._Turnier__playerSearch(name)
            self.assertSetEqual(set(disciplines), set(player.discipline_record.keys()))
            self.assertGreaterEqual(min(player.discipline_record.values()), max(player.discipline_record.values()) - 1)

        turnier.remove_player("k")
        names.remove("k")
        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)

        turnier.remove_player("j")
        names.remove("j")
        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)

        turnier.remove_player("i")
        names.remove("i")
        draw = turnier.make_multi_discipline_draw()
        self.__validate_multi_draw(draw, names, disciplines)
    
    def test_enter_remove_result(self):
        spieler = [('a', 0), ('b', 1), ('c', 2), ('d', 0)]
        turnier = Turnier(spieler)

        self.assertRaises(ValueError, turnier.enter_result, "a", "b", "c", "asdf", 1, 3)
        self.assertRaises(ValueError, turnier.enter_result, "a", "b", "c", "d", 0, 1)
        self.assertRaises(ValueError, turnier.enter_result, "a", "b", "c", "d", 1, -1)

        turnier.enter_result("a", "b", "c", "d", 1, 2)
        turnier.enter_result("a", "c", "d", "b", 1, 3)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')
        c = turnier._Turnier__playerSearch('c')
        d = turnier._Turnier__playerSearch('d')

        self.assertEqual(a.seed, 2)
        self.assertListEqual(sorted(a.partner), sorted(["b", "c"]))
        self.assertListEqual(sorted(a.gegner1), sorted(["c", "d"]))
        self.assertListEqual(sorted(a.gegner2), sorted(["d", "b"]))
        self.assertListEqual(sorted(a.gewonnen_gegen), sorted(["c", "d", "d", "b"]))
        self.assertListEqual(sorted(a.gewonnen_mit), sorted(["b", "c"]))
        self.assertEqual(a.punktedifferenz, 5)

        self.assertEqual(b.seed, 2)
        self.assertListEqual(sorted(b.partner), sorted(["a", "d"]))
        self.assertListEqual(sorted(b.gegner1), sorted(["c", "d"]))
        self.assertListEqual(sorted(b.gegner2), sorted(["c", "a"]))
        self.assertListEqual(sorted(b.gewonnen_gegen), sorted(["c", "d"]))
        self.assertListEqual(b.gewonnen_mit, ["a"])
        self.assertEqual(b.punktedifferenz, -1)

        self.assertEqual(c.seed, 3)
        self.assertListEqual(sorted(c.partner), sorted(["d", "a"]))
        self.assertListEqual(sorted(c.gegner1), sorted(["a", "b"]))
        self.assertListEqual(sorted(c.gegner2), sorted(["b", "d"]))
        self.assertListEqual(sorted(c.gewonnen_gegen), sorted(["d", "b"]))
        self.assertListEqual(c.gewonnen_mit, ["a"])
        self.assertEqual(c.punktedifferenz, 1)

        self.assertEqual(d.seed, 0)
        self.assertListEqual(sorted(d.partner), sorted(["c", "b"]))
        self.assertListEqual(sorted(d.gegner1), sorted(["b", "a"]))
        self.assertListEqual(sorted(d.gegner2), sorted(["a", "c"]))
        self.assertEqual(len(d.gewonnen_gegen), 0)
        self.assertEqual(len(d.gewonnen_mit), 0)
        self.assertEqual(d.punktedifferenz, -5)

        ranking = turnier.calculate_ranking()
        json.dumps(ranking)
        expected_ranking = [{'Name': 'a', 'Siege': 2, 'Punkte': 0, 'Punktedifferenz': 5, "removed": False, "rankingValue": 2000005001, 'Rang': 1},
                            {'Name': 'c', 'Siege': 1, 'Punkte': -1, 'Punktedifferenz': 1, "removed": False, "rankingValue": 999001001, 'Rang': 2},
                            {'Name': 'b', 'Siege': 1, 'Punkte': -1, 'Punktedifferenz': -1, "removed": False, "rankingValue": 998999001, 'Rang': 3},
                            {'Name': 'd', 'Siege': 0, 'Punkte': 0, 'Punktedifferenz': -5, "removed": False, "rankingValue": -4999, 'Rang': 4}]
        self.assertListEqual(ranking, expected_ranking)

        self.assertRaises(ValueError, turnier.remove_result, "a", "b", "c", "asdf", 1, 3)
        self.assertRaises(ValueError, turnier.remove_result, "a", "b", "c", "d", 0, 1)
        self.assertRaises(ValueError, turnier.remove_result, "a", "b", "c", "d", 1, -1)

        turnier.remove_result("a", "c", "d", "b", 1, 3)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')
        c = turnier._Turnier__playerSearch('c')
        d = turnier._Turnier__playerSearch('d')

        self.assertEqual(a.seed, 1)
        self.assertListEqual(a.partner, ["b"])
        self.assertListEqual(a.gegner1, ["c"])
        self.assertListEqual(a.gegner2, ["d"])
        self.assertListEqual(sorted(a.gewonnen_gegen), sorted(["c", "d"]))
        self.assertListEqual(a.gewonnen_mit, ["b"])
        self.assertEqual(a.punktedifferenz, 2)

        self.assertEqual(b.seed, 2)
        self.assertListEqual(b.partner, ["a"])
        self.assertListEqual(b.gegner1, ["d"])
        self.assertListEqual(b.gegner2, ["c"])
        self.assertListEqual(sorted(b.gewonnen_gegen), sorted(["c", "d"]))
        self.assertListEqual(b.gewonnen_mit, ["a"])
        self.assertEqual(b.punktedifferenz, 2)

        self.assertEqual(c.seed, 2)
        self.assertListEqual(c.partner, ["d"])
        self.assertListEqual(c.gegner1, ["a"])
        self.assertListEqual(c.gegner2, ["b"])
        self.assertEqual(len(c.gewonnen_gegen), 0)
        self.assertEqual(len(c.gewonnen_mit), 0)
        self.assertEqual(c.punktedifferenz, -2)

        self.assertEqual(d.seed, 0)
        self.assertListEqual(d.partner, ["c"])
        self.assertListEqual(d.gegner1, ["b"])
        self.assertListEqual(d.gegner2, ["a"])
        self.assertEqual(len(d.gewonnen_gegen), 0)
        self.assertEqual(len(d.gewonnen_mit), 0)
        self.assertEqual(d.punktedifferenz, -2)

    def test_enter_remove_singles(self):
        spieler = [('a', 0), ('b', 1), ('c', 2), ('d', 0)]
        turnier = Turnier(spieler)
        self.assertEqual(turnier.einzel, 0)

        self.assertRaises(ValueError, turnier.enter_result_Singles, "a", "asdf", 1, 1)
        self.assertRaises(ValueError, turnier.enter_result_Singles, "a", "b", 3, 1)
        self.assertRaises(ValueError, turnier.enter_result_Singles, "a", "b", 1, -4)

        turnier.enter_result_Singles('a', 'b', 1, 2)
        turnier.enter_result_Singles('a', 'd', 2, 3)
        turnier.enter_result('a', 'b', 'c', 'd', 1, 4)
        self.assertEqual(turnier.einzel, 2)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')
        c = turnier._Turnier__playerSearch('c')
        d = turnier._Turnier__playerSearch('d')

        self.assertEqual(a.seed, 2)
        self.assertListEqual(sorted(a.einzelGegner), sorted(["b", "d"]))
        self.assertListEqual(sorted(a.gewonnen_gegen), sorted(["b", "c", "d"]))
        self.assertEqual(a.punktedifferenz, 3)
        self.assertEqual(a.einzel, 2)

        self.assertEqual(b.seed, 2)
        self.assertListEqual(b.einzelGegner, ["a"])
        self.assertListEqual(sorted(b.gewonnen_gegen), sorted(["c", "d"]))
        self.assertEqual(b.punktedifferenz, 2)
        self.assertEqual(b.einzel, 1)

        self.assertEqual(c.seed, 2)
        self.assertEqual(len(c.einzelGegner), 0)
        self.assertEqual(len(c.gewonnen_gegen), 0)
        self.assertEqual(c.punktedifferenz, -4)
        self.assertEqual(c.einzel, 0)

        self.assertEqual(d.seed, 1)
        self.assertListEqual(d.einzelGegner, ["a"])
        self.assertListEqual(d.gewonnen_gegen, ["a"])
        self.assertEqual(d.punktedifferenz, -1)
        self.assertEqual(d.einzel, 1)

        self.assertRaises(ValueError, turnier.remove_result_Singles, "a", "asdf", 1, 3)
        self.assertRaises(ValueError, turnier.remove_result_Singles, "a", "b", 0, 1)
        self.assertRaises(ValueError, turnier.remove_result_Singles, "a", "b", 1, -1)

        turnier.remove_result_Singles('a', 'b', 1, 2)
        self.assertEqual(turnier.einzel, 1)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')
        c = turnier._Turnier__playerSearch('c')
        d = turnier._Turnier__playerSearch('d')

        self.assertEqual(a.seed, 1)
        self.assertListEqual(a.einzelGegner, ["d"])
        self.assertListEqual(sorted(a.gewonnen_gegen), sorted(["c", "d"]))
        self.assertEqual(a.punktedifferenz, 1)
        self.assertEqual(a.einzel, 1)

        self.assertEqual(b.seed, 2)
        self.assertEqual(len(b.einzelGegner), 0)
        self.assertListEqual(sorted(b.gewonnen_gegen), sorted(["c", "d"]))
        self.assertEqual(b.punktedifferenz, 4)
        self.assertEqual(b.einzel, 0)

        self.assertEqual(c.seed, 2)
        self.assertEqual(len(c.einzelGegner), 0)
        self.assertEqual(len(c.gewonnen_gegen), 0)
        self.assertEqual(c.punktedifferenz, -4)
        self.assertEqual(c.einzel, 0)

        self.assertEqual(d.seed, 1)
        self.assertListEqual(d.einzelGegner, ["a"])
        self.assertListEqual(d.gewonnen_gegen, ["a"])
        self.assertEqual(d.punktedifferenz, -1)
        self.assertEqual(d.einzel, 1)

        ranking = turnier.calculate_ranking()
        json.dumps(ranking)
        expected_ranking = [{'Name': 'd', 'Siege': 1, 'Punkte': 1, 'Punktedifferenz': -1, 'removed': False, "rankingValue": 1000999001, 'Rang': 1},
                           {'Name': 'b', 'Siege': 1, 'Punkte': 0, 'Punktedifferenz': 4, 'removed': False, "rankingValue": 1000004001, 'Rang': 2},
                           {'Name': 'a', 'Siege': 1, 'Punkte': 0, 'Punktedifferenz': 1, 'removed': False, "rankingValue": 1000001001, 'Rang': 3},
                           {'Name': 'c', 'Siege': 0, 'Punkte': 0, 'Punktedifferenz': -4, 'removed': False, "rankingValue": -3999, 'Rang': 4}]
        self.assertListEqual(ranking, expected_ranking)
        
    def test_enter_remove_freilos(self):
        spieler = [('a', 0), ('b', 1)]
        turnier = Turnier(spieler)
        self.assertEqual(turnier.freilose, 0)

        self.assertRaises(ValueError, turnier.enter_result_Freilos, "asdf")

        turnier.enter_result_Freilos('b')
        turnier.enter_result_Freilos('a')
        self.assertEqual(turnier.freilose, 2)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')

        self.assertEqual(a.seed, 1)
        self.assertEqual(a.freilose, 1)
        self.assertEqual(b.seed, 2)
        self.assertEqual(b.freilose, 1)

        self.assertRaises(ValueError, turnier.remove_result_Freilos, "asdf")

        turnier.remove_result_Freilos('b')
        self.assertEqual(turnier.freilose, 1)
        a = turnier._Turnier__playerSearch('a')
        b = turnier._Turnier__playerSearch('b')

        self.assertEqual(a.seed, 1)
        self.assertEqual(a.freilose, 1)
        self.assertEqual(b.seed, 1)
        self.assertEqual(b.freilose, 0)

        ranking = turnier.calculate_ranking()
        json.dumps(ranking)
        expected_ranking = [{"Name": "a", "Siege": 1, "Punkte": 0, "Punktedifferenz": 0, 'removed': False, "rankingValue": 1000000001, 'Rang': 1},
                           {"Name": "b", "Siege": 0, "Punkte": 0, "Punktedifferenz": 0, 'removed': False, "rankingValue": 1, 'Rang': 2}]
        self.assertListEqual(ranking, expected_ranking)

    def test_save_to_dict(self):
        spieler = [('a', 0), ('b', 1), ('c', 2), ('d', 0)]
        turnier = Turnier(spieler)
        turnier.enter_result("a", "b", "c", "d", 1, 2)
        turnier.enter_result("a", "c", "d", "b", 1, 3)
        turnier.enter_result_Singles('a', 'b', 1, 2)
        turnier.enter_result_Singles('a', 'd', 2, 3)
        turnier.enter_result_Freilos('b')
        turnier.enter_result_Freilos('a')
        turnier.remove_player("a")

        dict_repr = turnier.to_dict()
        json.dumps(dict_repr)

        replica = Turnier()
        replica.from_dict(dict_repr)

        self.assertEqual(turnier.freilose, replica.freilose)
        self.assertEqual(turnier.einzel, replica.einzel)
        self.assertEqual(len(turnier.spieler), len(replica.spieler))
        self.assertIn('spieler', dict_repr)
        for spieler in replica.spieler:
            self.assertIn(spieler.to_dict(), dict_repr['spieler'])
        self.assertIn('removed', dict_repr)
        for spieler in replica.removed:
            self.assertIn(spieler.to_dict(), dict_repr['removed'])


if __name__ == '__main__':
    import unittest
    unittest.main()
