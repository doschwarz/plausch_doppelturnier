# The Social Tournament Software

Organize fun multi-category tournaments effortlessly with this app! Get to know everyone at the party by playing with a different partner in every new round! Organize:

* Social tournaments for any racket sport that has a doubles category, like Tennis, Badminton, or Tabletennis.
* Bar tournaments, for example a combination of beerpong and foosball.

The app designed for 2v2 game modes with no draws. In every round, you get a new partner and different opponents. The app tries to create even matches based on player performance throughout the tournament, while also having players of different skill levels play together sometimes.

Fully automated: If the number of players is not divisible by 4, it automatically assigns a singles game and/or a bye. If you organize a tournament with multiple categories, it ensures that players play about the same number of games in each category. At the end, players will be ranked according to individual performance.

## Usage

Use the app through an interactive website, or locally with iPython or in Jupyter notebook.

### Using the Interactive Website

#### Free online version

The app is deployed to the Azure App Service, available here for free usage:

[tournament-software.azurewebsites.net](https://tournament-software.azurewebsites.net/)

Note that the container is set to sleep mode if it has not been in use for a while. So sometimes, the first website access may take up to 30 seconds.

#### Run locally inside Docker container

You can also run the app locally by getting the docker image here:

```bash
docker pull doschwarz/tournament-software:latest
docker container run --name ts -p 8000:8000 doschwarz/tournament-software:latest 
```

Open your browser and access `http://localhost:8000/`

Stop/restart the container like so:

```bash
docker container stop ts
docker container start ts
```

#### Install locally and run with Django development server

Finally, you can also clone the code directly from this repository, install the requirements with `pip install -r requirements.txt`, and run Django locally:

```bash
python3 app/manage.py runserver
```

### Running inside a Jupyter notebook or with iPython

(1) import `Turnier` class

```python
from engine import Turnier
```

(2) Make a list with player names and (optionally) player strength. Player strength must be an integer &geq; 0; the larger, the stronger the player is. This is used to create about even matches at the beginning of the tournament.

```python
players = ["Thomas", "Lisa", ...]
players = [("Thomas", 0), ("Lisa", 1), ...]
```

(3) Initialize tournament

```python
my_fun_tournament = Turnier(players)
```

(4) Make draw. The output is only a suggestion. You are not obligated to actually play the round in the suggested configuration. This design gives the tournament organizer an uncomplicated way to adjust the matches should need be.

```python
my_fun_tournament.auslosung()
```

(5) Enter results; see `engine.py` for documentation.

```python
# Thomas + Lisa vs. Andreas + Bin: 21 - 16
my_fun_tournament.enter_result("Thomas", "Lisa", "Andreas", "Bin", 1, 5)
# Einzel: Andreas vs Bin: 15 - 16
my_fun_tournament.enter_result_Singles("Andreas", "Bin", 2, 1)
# Freilos: Thomas
my_fun_tournament.enter_result_Freilos("Thomas")
```

(6) Compute ranking

```python
my_fun_tournament.show_ranking()
```
