FROM python:3.8.3-slim

WORKDIR /usr/src/app

COPY ./requirements.txt ./requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY ./app/ ./app/
COPY ./engine.py ./engine.py
COPY ./engine_test.py ./engine_test.py

RUN /usr/local/bin/python3 -m pytest  

CMD cd /usr/src/app/app; /usr/local/bin/python3 manage.py runserver 0.0.0.0:8000
